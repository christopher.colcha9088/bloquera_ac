<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trabajadores extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model("trabajador");
  }

  public function index()
  {
    $data["listadoTrabajador"]=$this->trabajador->obtenerTodos();
    $this->load->view("header");
    $this->load->view("trabajadores/index",$data);
    $this->load->view("footer");
  }

  public function guardar()
  {
    $datosTrabajador=array(
    "nombre_trab"=>$this->input->post('nombre_trab'),
    "apellido_trab"=>$this->input->post('apellido_trab'),
    "dias_asis_trab"=>$this->input->post('dias_asis_trab'),
    "dias_inasis_trab"=>$this->input->post('dias_inasis_trab'),
    "sueldo_trab"=>$this->input->post('sueldo_trab'),
    "telefono_trab"=>$this->input->post('telefono_trab'),
    "cedula_trab"=>$this->input->post('cedula_trab')
    );

    if ($this->trabajador->insertar($datosTrabajador))
    {
      $resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
    }
    else
    {
      $resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}



  public function borrar($id_trab)
  {
    $data=$this->trabajador->obtenerPorId($id_trab);
    if ($this->trabajador->eliminarPorId($id_trab))
    {
      $this->session->set_flashdata('confirmacion','Estudiante ELIMINADO exitosamente');
      enviarEmail("christopher.colca9088@utc.edu.ec", "NOTIFICACION_ELIMINACION","<h1>La empresa ".$data->nombre_empresa_ac." ".$data->telefono_ac.
     "</h1> fue eliminado el ".date("Y-m-d H:i:s"));
    }
    else
    {
      $this->session->set_flashdata('error','Error al ELIMINAR, verifique e intente de nuevo');
    }
    redirect('trabajadores/index');
  }


  public function editar($id)
  {
    $data["trabajador"]=$this->trabajador->obtenerPorId($id);
    $this->load->view("trabajadores/editar",$data);
  }

	public function trabajadorEditar()
	{
			$datosTrabajadorEditado=array(
	    "nombre_trab"=>$this->input->post('nombre_trab'),
	    "apellido_trab"=>$this->input->post('apellido_trab'),
	    "dias_asis_trab"=>$this->input->post('dias_asis_trab'),
	    "dias_inasis_trab"=>$this->input->post('dias_inasis_trab'),
	    "sueldo_trab"=>$this->input->post('sueldo_trab'),
	    "telefono_trab"=>$this->input->post('telefono_trab'),
	    "cedula_trab"=>$this->input->post('cedula_trab')
			);
		$id=$this->input->post("id_trab");

		if($this->trabajador->actualizar($id,$datosTrabajadorEditado)) {
		redirect('trabajadores/index');
		}
		else
		{
			echo "Error";
		}
	}




}
