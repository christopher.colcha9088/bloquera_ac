<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facturas extends CI_Controller
{
	public function __construct()
	{
					parent::__construct();
					$this->load->model("factura");
					$this->load->model("Vendedor");
					$this->load->model("Cantidad");
					$this->load->model("producto");
				}
	// InvalidArgumentException
  public function index()
	{
    $data["listadoProducto"]=$this->producto->obtenerTodos();
		$data["listadoVendedor"]=$this->Vendedor->obtenerTodos();
		$data["listadoCantidad"]=$this->Cantidad->obtenertodos();
    $this->load->view("header");
    $this->load->view("facturas/index",$data);
    $this->load->view("footer");
  }
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardar()
	{
		$datosFactura=array(
    "fk_id_produc"=>$this->input->post('fk_id_produc'),
    "fk_id_ven"=>$this->input->post('fk_id_ven'),
    "fk_id_cant"=>$this->input->post('fk_id_cant'),
    "nombre_fac"=>$this->input->post('nombre_fac'),
    "direccion_fac"=>$this->input->post('direccion_fac'),
    "cedula_fac"=>$this->input->post('cedula_fac'),
    "fecha_fac"=>$this->input->post('fecha_fac')
		);

		if($this->factura->insertar($datosFactura))
		{
			$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
		}
		else
		{
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}


//borrar profesor
	public function borrar($id_fac)
	{
		$data=$this->factura->ObtenerPorId($id_fac);
		if($this->factura->eliminarPorId($id_fac))
		{
			$resultado=array("estado"=>"ok", "mensaje"=>"eliminado exitoso");
		}else{
			$resultado=array("estado"=>"error");
		}
		redirect('facturas/index');
	}

  // editar
  public function editar($id)
  {
	$data["factura"]=$this->factura->ObtenerPorId($id);
  $data["producto"]=$this->producto->obtenerTodos();
	$data["vendedor"]=$this->Vendedor->obtenerTodos();
	$data["Cantidad"]=$this->Cantidad->obtenerTodos();
  $this->load->view('facturas/editar',$data);
  }

	public function facturaEditar()
	{
			$datosFacturaEditado=array(
      "fk_id_produc"=>$this->input->post('fk_id_produc'),
      "fk_id_ven"=>$this->input->post('fk_id_ven'),
      "fk_id_cant"=>$this->input->post('fk_id_cant'),
      "nombre_fac"=>$this->input->post('nombre_fac'),
      "direccion_fac"=>$this->input->post('direccion_fac'),
      "cedula_fac"=>$this->input->post('cedula_fac'),
      "fecha_fac"=>$this->input->post('fecha_fac')
			);
	$id=$this->input->post("id_fac");

	if($this->factura->actualizar($id,$datosFacturaEditado)) {
		redirect('facturas/index');
	}else{
		echo "Error";
	}

}


  	public function listado ()
  	{
			$data["listadoFactura"]=$this->factura->obtenerTodos();
  		$this->load->view("facturas/listado",$data);
  	}




}//cierre de la clase no borrar despues estas en problemas te conozco
