<?php
 class Producciones extends CI_Controller
 {
 	public function __construct()
	{
		parent::__construct();
		$this->load->model("produccion");
		$this->load->model("producto");
	}
	//renderizacion de la primera vista de index base de datos
	public function index()
	{
    $data["listadoProducciones"]=$this->produccion->obtenerTodas();
    $this->load->view("header");
		$this->load->view("producciones/index",$data);
		$this->load->view("footer");
 	}
//renderizacion de la primera vista de nuevo base de datos
  public function nuevo()
	{
    $data["listadoProducto"]=$this->producto->obtenerTodos();
 		$this->load->view("header");
		$this->load->view("producciones/nuevo",$data);
		$this->load->view("footer");
 	}

  public function guardarProduccion()
  {
    $datos=array
    (
    "fk_id_pro_ac"=>$this->input->post("fk_id_pro_ac"),
    "cantidad_pro"=>$this->input->post("cantidad_pro"),
    "mes_pro"=>$this->input->post("mes_pro"),
    "requerido_pro"=>$this->input->post("requerido_pro")
    );
    if ($this->produccion->insertar($datos))
    {
      $this->session->set_flashdata("confirmacion","Producion Isertada exitosamente");
    }
    else
    {
      $this->session->set_flashdata("error","Error al insertar, verigique e intente otra vez");
    }
    redirect("producciones/index");
  }

  // editar
  public function editar($id_pro){
    $data["produccion"]=$this->produccion->obtenerPorId($id_pro);
    $data["listadoProducto"]=$this->producto->obtenerTodos();
    $this->load->view("header");
    $this->load->view("producciones/editar",$data);
    $this->load->view("footer");
  }







	//renderizacion de la primera vista de Produciones


  public function actualizarProduccion(){
		$datos=array(
    "fk_id_pro_ac"=>$this->input->post("fk_id_pro_ac"),
    "cantidad_pro"=>$this->input->post("cantidad_pro"),
    "requerido_pro"=>$this->input->post("requerido_pro"),
    "fecha_matr"=>$this->input->post("fecha_matr")
		);
		$id_pro=$this->input->post("id_pro");
		if($this->produccion->actualizar($id_pro,$datos)){
				$this->session->set_flashdata("confirmacion",
			  "produccion Actulizada Exitosamente");
				redirect("producciones/index");
		}else{
			$this->session->set_flashdata("error",
			"Error al actualizar, verifique e intente otra vez");
			redirect("producciones/editar/".$id_pro);
		}

	}
  public function borrar($id_pro){
  	if ($this->produccion->eliminarPorId($id_pro)) {
  		$this->session->set_flashdata('confirmacion', 'Matricula borrado exitosamente');
  	} else {
  		$this->session->set_flashdata('error','Error al ELIMINAR intente de nuevo');
  	}
  	redirect('producciones/index');

  }
    }//cierre de la clase no borrar despues estas en problemas te conozco
