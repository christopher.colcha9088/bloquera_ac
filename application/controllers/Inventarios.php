<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventarios extends CI_Controller
{
	public function __construct()
	{
					parent::__construct();
					$this->load->model("inventario");
				}
				//renderiza la vista index de estudiantes

	// InvalidArgumentException
  public function index()
	{
		$data["listadoInventario"]=$this->inventario->obtenerTodos();
    $this->load->view("header");
    $this->load->view("inventarios/index",$data);
    $this->load->view("footer");
  }

// editar
public function editar($id)
{
	$data["inventario"]=$this->inventario->ObtenerPorId($id);
	$this->load->view('inventarios/editar',$data);
}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardar()
	{
		$datosInventario=array(
	    "arena_inv"=>$this->input->post('arena_inv'),
	    "piedra_inv"=>$this->input->post('piedra_inv'),
	    "agua_inv"=>$this->input->post('agua_inv')
		);

		if($this->inventario->insertar($datosInventario))
		{
			$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
		}else{
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}

	public function listado ()
	{
		$data["Inventario"]=$this->inventario->obtenerTodos();
		$this->load->view("inventarios/listado",$data);
	}

//borrar profesor
	public function borrar($id_inv)
	{
		$data=$this->inventario->ObtenerPorId($id_inv);
		if($this->inventario->eliminarPorId($id_inv))
		{
			$this->session->set_flashdata('confirmacion', 'Borrado exitosamente');
		}
		else
		{
			$this->session->set_flashdata('error','Error al ELIMINAR intente de nuevo');
		}
		redirect('inventarios/index');
	}

	public function inventarioEditar()
	{
			$datosInventarioEditado=array(
		    "id_inv"=>$this->input->post('id_inv'),
  	    "arena_inv"=>$this->input->post('arena_inv'),
  	    "piedra_inv"=>$this->input->post('piedra_inv'),
  	    "agua_inv"=>$this->input->post('agua_inv')
			);
	$id=$this->input->post("id_inv");

	if($this->inventario->actualizar($id,$datosInventarioEditado)) {
	redirect('inventarios/index');
	}
	else
	{
		echo "Error";
	}
}





}//cierre de la clase no borrar despues estas en problemas te conozco
