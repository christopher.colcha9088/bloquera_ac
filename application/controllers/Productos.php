<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller
{
	public function __construct()
	{
					parent::__construct();
					$this->load->model("producto");
				}
				//renderiza la vista index de estudiantes

	// InvalidArgumentException
  public function index()
	{
		$data["listadoProducto"]=$this->producto->obtenerTodos();
    $this->load->view("header");
    $this->load->view("productos/index",$data);
    $this->load->view("footer");
  }
// asignaturas



	//funcion para capturar los valores del
	//formulario nuevo
	public function guardar(){
		$datosProducto=array(
	    "nombre_produc"=>$this->input->post('nombre_produc'),
	    "cantidad_produc"=>$this->input->post('cantidad_produc'),
	    "tamaño_produc"=>$this->input->post('tamaño_produc')
		);
		if($this->producto->insertar($datosProducto)){
			$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
		}else{
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}

//borrar profesor

public function borrar($id_produc)
	{
		$data=$this->producto->ObtenerPorId($id_produc);
		if($this->producto->eliminarPorId($id_produc))
		{
			$resultado=array("estado"=>"ok", "mensaje"=>"eliminado exitoso");
			enviarEmail("christopher.colcha9088@utc.edu.ec", "NOTIFICACION ELIMINACION","<h1> el producto ".$data->nombre_produc." ".$data->cantidad_produc.
		 "</h1> fue eliminado el ".date("Y-m-d H:i:s")
			);
		}else{
			$resultado=array("estado"=>"error");
		}
		redirect('productos/index');
	}

	public function editar($id)
	{
		$data["productoEditar"]=$this->producto->ObtenerPorId($id);
			$this->load->view('header');
			$this->load->view('productos/editar',$data);
			$this->load->view('footer');
	}
	public function productoEditar()
	{
			$datosProductoEditado=array(
		    "id_produc"=>$this->input->post('id_produc'),
		    "nombre_produc"=>$this->input->post('nombre_produc'),
		    "cantidad_produc"=>$this->input->post('cantidad_produc'),
		    "tamaño_produc"=>$this->input->post('tamaño_produc')
			);
	$id=$this->input->post("id_produc");

	if($this->producto->actualizar($id,$datosProductoEditado)) {
	redirect('productos/index');
	}else {
		echo "Error";
}
}





}//cierre de la clase no borrar despues estas en problemas te conozco
