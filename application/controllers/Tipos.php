<?php
defined('BASEPATH') OR EXIT ('No direct script access allowed');

class Tipos extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model("Tipo");
  }
  // renderizacion de la vista tipo
public function index(){
$data["listadotipo"]=$this->Tipo->obtenerTodos();
  $this->load->view("header");
  $this->load->view("tipos/index", $data);
  $this->load->view("footer");
}

public function guardar(){
$datostipo=array(
  "id_tip"=>$this->input->post("id_tip"),
  "tipo_tip"=>$this->input->post("tipo_tip"),
  "medida_tip"=>$this->input->post("medida_tip")
);
if($this->Tipo->insertar($datostipo)){
  $resultado=array("estado"=>"ok","mensaje"=>"ingresado con exito");
}else{
  $resultado=array("estado"=>"Error");
}
echo json_encode($resultado);
}
//borarr
public function borrar($id_tip){
  $data=$this->Tipo->obtenerPorId($id_tip);
	if($this->Tipo->eliminarPorId($id_tip)){
			$resultado=array("estado"=>"ok", "mensaje"=>"Eliminado Exitoso");
			enviarEmail("jhonatan.alarcon2767@utc.edu.ec", "Notificacion eliminacion tipo", "<h1> el tipo es  ".$data->tipo_tip." y su medida es ".$data->medida_tip.
			"<h1> Fue eliminado el ".date("Y-m-d H:i:s")
		);
  }else {
			$resultado=array("estado"=>"error");
		}
		redirect("tipos/index");
	}

	public function editar($id)
	{
		$data["tipo"]=$this->Tipo->ObtenerPorId($id);
		$this->load->view('tipos/editar',$data);
	}

	public function tipoEditar()
	{
			$datosTipo=array(
        "id_tip"=>$this->input->post("id_tip"),
        "tipo_tip"=>$this->input->post("tipo_tip"),
        "medida_tip"=>$this->input->post("medida_tip")
			);
			$id=$this->input->post("id_tip");

			if($this->Tipo->editar($id,$datosTipo)) {
				$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
			}else {
				$resultado=array("estado"=>"error");
		}
		redirect('tipos/index');

		}


}//no eliminar esta llave
