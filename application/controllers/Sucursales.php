<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucursales extends CI_Controller
{
	public function __construct()
	{
					parent::__construct();
					$this->load->model("sucursal");
				}
				//renderiza la vista index de estudiantes

	// InvalidArgumentException
  public function index()
	{
		$data["listadosucursal"]=$this->sucursal->obtenerTodos();
    $this->load->view("header");
    $this->load->view("sucursales/index",$data);
    $this->load->view("footer");
  }



	//funcion para capturar los valores del
	//formulario nuevo
	public function guardar()
	{
		$datosSucursal=array(
	    "provincia_ac"=>$this->input->post('provincia_ac'),
	    "ciudad_ac"=>$this->input->post('ciudad_ac'),
	    "estado_ac"=>$this->input->post('estado_ac'),
      "dirreccion_ac"=>$this->input->post('dirreccion_ac'),
	    "email_ac"=>$this->input->post('email_ac')
		);
		if($this->sucursal->insertar($datosSucursal))
		{
			$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
		}
		else
		{
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}

//borrar profesor
	public function borrar($id_suc_ac ){
		$data=$this->sucursal->ObtenerPorId($id_suc_ac);
		if($this->sucursal->eliminarPorId($id_suc_ac)){
			$resultado=array("estado"=>"ok", "mensaje"=>"Eliminado Exitoso");
			enviarEmail("jhonatan.alarcon2767@utc.edu.ec", "Notificacion eliminacion sucursales", "<h1> la sucursal".$data->provincia_ac."".$data->ciudad_ac.
			"<h1> Fue eliminado el ".date("Y-m-d H:i:s")
		); }else {
			$resultado=array("estado"=>"error");
		}
		redirect("sucursales/index");
	}

	public function editar($id)
	{
		$data["sucursal"]=$this->sucursal->ObtenerPorId($id);
		$this->load->view('sucursales/editar',$data);
	}

	public function sucursalEditar()
	{
			$datosSucursalEditado=array(
        "id_suc_ac"=>$this->input->post('id_suc_ac'),
        "provincia_ac"=>$this->input->post('provincia_ac'),
  	    "ciudad_ac"=>$this->input->post('ciudad_ac'),
  	    "estado_ac"=>$this->input->post('estado_ac'),
        "dirreccion_ac"=>$this->input->post('dirreccion_ac'),
  	    "email_ac"=>$this->input->post('email_ac')
			);
			$id=$this->input->post("id_suc_ac");

			if($this->sucursal->editar($id,$datosSucursalEditado)) {
				$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
			}else {
				$resultado=array("estado"=>"error");
		}
		redirect('sucursales/index');

		}




}//cierre de la clase no borrar despues estas en problemas te conozco
