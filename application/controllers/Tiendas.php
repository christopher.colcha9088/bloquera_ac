<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiendas extends CI_Controller
{
	public function __construct()
	{
					parent::__construct();
					$this->load->model("tienda");
					$this->load->model("Vendedor");
					$this->load->model("producto");
				}
	// InvalidArgumentException
  public function index()
	{
    $data["listadoProducto"]=$this->producto->obtenerTodos();
		$data["listadoVendedor"]=$this->Vendedor->obtenerTodos();
		$data["listadoTienda"]=$this->tienda->obtenerTodos();
    $this->load->view("header");
    $this->load->view("tiendas/index",$data);
    $this->load->view("footer");
  }
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardar()
	{
		$datosTienda=array(
	    "fk_id_ven"=>$this->input->post('fk_id_ven'),
		  "fk_id_produc"=>$this->input->post('fk_id_produc'),
	    "fecha_tie"=>$this->input->post('fecha_tie')
		);

		if($this->tienda->insertar($datosTienda))
		{
			$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
		}
		else
		{
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}

//borrar profesor
	public function borrar($id_tie)
	{
		$data=$this->tienda->ObtenerPorId($id_tie);
		if($this->tienda->eliminarPorId($id_tie))
		{
			$resultado=array("estado"=>"ok", "mensaje"=>"eliminado exitoso");
		}else{
			$resultado=array("estado"=>"error");
		}
		redirect('tiendas/index');
	}

  // editar
  public function editar($id)
  {
	$data["tienda"]=$this->tienda->ObtenerPorId($id);
  $data["producto"]=$this->producto->obtenerTodos();
	$data["vendedor"]=$this->Vendedor->obtenerTodos();
  $this->load->view('tiendas/editar',$data);
  }

	public function tiendaEditar()
	{
			$datosTiendaEditado=array(
			"fk_id_produc"=>$this->input->post('fk_id_produc'),
  	    "fk_id_ven"=>$this->input->post('fk_id_ven'),
  	    "fecha_tie"=>$this->input->post('fecha_tie')
			);
	$id=$this->input->post("id_tie");

	if($this->tienda->actualizar($id,$datosTiendaEditado)) {
		$resultado=array("estado"=>"ok", "mensaje"=>"eliminado exitoso");
	}else{
		$resultado=array("estado"=>"error");
	}
	echo json_encode($resultado);
}





}//cierre de la clase no borrar despues estas en problemas te conozco
