<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller
{
	public function __construct()
	{
					parent::__construct();
					$this->load->model("configuracion");
				}
				//renderiza la vista index de estudiantes

	// InvalidArgumentException
  public function index()
	{
		$data["listadoConfiguracion"]=$this->configuracion->obtenerTodos();
    $this->load->view("header");
    $this->load->view("configuraciones/index",$data);
    $this->load->view("footer");
  }

// editar
public function editar($id)
{
	$data["configuracion"]=$this->configuracion->ObtenerPorId($id);
	$this->load->view('configuraciones/editar',$data);
}
	//funcion para capturar los valores del
	//formulario nuevo
	public function guardar()
	{
		$datosConfiguracion=array(
	    "nombre_empresa_ac"=>$this->input->post('nombre_empresa_ac'),
	    "ruc_ac"=>$this->input->post('ruc_ac'),
	    "telefono_ac"=>$this->input->post('telefono_ac'),
	    "dirreccion_ac"=>$this->input->post('dirreccion_ac'),
	    "representante_ac "=>$this->input->post('representante_ac')
		);

		if($this->configuracion->insertar($datosConfiguracion))
		{
			$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
		}else{
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}

//borrar profesor
	public function borrar($id_con_ac)
	{
		$data=$this->configuracion->ObtenerPorId($id_con_ac);
		if($this->configuracion->eliminarPorId($id_con_ac))
		{
			$resultado=array("estado"=>"ok", "mensaje"=>"eliminado exitoso");
			// enviarEmail("christopher.colca9088@utc.edu.ec", "NOTIFICACION_ELIMINACION","<h1>La empresa ".$data->nombre_empresa_ac." ".$data->telefono_ac.
		 // "</h1> fue eliminado el ".date("Y-m-d H:i:s")
			// );
		}else{
			$resultado=array("estado"=>"error");
		}
		echo json_encode($resultado);
	}

	public function configuracionEditar()
	{
			$datosConfiguracionEditado=array(
		    "id_con_ac"=>$this->input->post('id_con_ac'),
  	    "nombre_empresa_ac"=>$this->input->post('nombre_empresa_ac'),
  	    "ruc_ac"=>$this->input->post('ruc_ac'),
  	    "telefono_ac"=>$this->input->post('telefono_ac'),
  	    "dirreccion_ac"=>$this->input->post('dirreccion_ac'),
  	    "representante_ac "=>$this->input->post('representante_ac')
			);
	$id=$this->input->post("id_con_ac");

	if($this->configuracion->actualizar($id,$datosConfiguracionEditado)) {
	redirect('configuraciones/index');
	}
	else
	{
		echo "Error";
	}
}





}//cierre de la clase no borrar despues estas en problemas te conozco
