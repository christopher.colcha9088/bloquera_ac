<?php
defined('BASEPATH') OR EXIT ('No direct script access allowed');

class Vendedores extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model("Vendedor");
  }
  // renderizacion de la vista cantidad
public function index(){
  $data["listadovendedores"]=$this->Vendedor->obtenerTodos();
  $this->load->view("header");
  $this->load->view("vendedores/index",$data);
  $this->load->view("footer");
}

public function guardar(){
$datosVendedor=array(
  "id_ven"=>$this->input->post("id_ven"),
  "nombre_ven"=>$this->input->post("nombre_ven"),
  "apellido_ven"=>$this->input->post("apellido_ven")
);
if($this->Vendedor->insertar($datosVendedor)){
  $resultado=array("estado"=>"ok","mensaje"=>"ingresado con exito");
}else{
  $resultado=array("estado"=>"Error");
}
echo json_encode($resultado);
}
//borarr
public function borrar($id_ven){
		$data=$this->Vendedor->obtenerPorId($id_ven);
		if($this->Vendedor->eliminarPorId($id_ven)){
			$resultado=array("estado"=>"ok", "mensaje"=>"Eliminado Exitoso");
			enviarEmail("jhonatan.alarcon2767@utc.edu.ec", "Notificacion eliminacion Vendedor", "<h1> el nombre ".$data->nombre_ven." y con apellido ".$data->apellido_ven.
			"<h1> Fue eliminado el ".date("Y-m-d H:i:s")
		);
  }else {
			$resultado=array("estado"=>"error");
		}
		redirect("vendedores/index");
	}

	public function editar($id)
	{
		$data["vendedor"]=$this->Vendedor->ObtenerPorId($id);
		$this->load->view('vendedores/editar',$data);
	}

	public function vendedorEditar()
	{
			$datosVendedor=array(
        "id_ven"=>$this->input->post("id_ven"),
        "nombre_ven"=>$this->input->post("nombre_ven"),
        "apellido_ven"=>$this->input->post("apellido_ven")
			);
			$id=$this->input->post("id_ven");

			if($this->Vendedor->editar($id,$datosVendedor)) {
				$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
			}else {
				$resultado=array("estado"=>"error");
		}
		redirect('vendedores/index');

		}


}//no eliminar esta llave
