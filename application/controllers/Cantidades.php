<?php
defined('BASEPATH') OR EXIT ('No direct script access allowed');

class Cantidades extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model("Cantidad");
  }
  // renderizacion de la vista cantidad
public function index(){
  $data["listadoCantidad"]=$this->Cantidad->obtenerTodos();
  $this->load->view("header");
  $this->load->view("cantidades/index",$data);
  $this->load->view("footer");
}

public function guardar(){
$datosCantidad=array(
  "id_cant"=>$this->input->post("id_cant"),
  "unitario_cant"=>$this->input->post("unitario_cant"),
  "paleta_cant"=>$this->input->post("paleta_cant")
);
if($this->Cantidad->insertar($datosCantidad)){
  $resultado=array("estado"=>"ok","mensaje"=>"ingresado con exito");
}else{
  $resultado=array("estado"=>"Error");
}
echo json_encode($resultado);
}
//borarr
public function borrar($id_cant){
		$data=$this->Cantidad->obtenerPorId($id_cant);
		if($this->Cantidad->eliminarPorId($id_cant)){
			$resultado=array("estado"=>"ok", "mensaje"=>"Eliminado Exitoso");
			enviarEmail("jhonatan.alarcon2767@utc.edu.ec", "Notificacion eliminacion cantidad", "<h1> la cantidad unitaria es de ".$data->unitario_cant." y en paleta ".$data->paleta_cant.
			"<h1> Fue eliminado el ".date("Y-m-d H:i:s")
		);
  }else {
			$resultado=array("estado"=>"error");
		}
		redirect("cantidades/index");
	}

	public function editar($id)
	{
		$data["cantidad"]=$this->Cantidad->ObtenerPorId($id);
		$this->load->view('cantidades/editar',$data);
	}

	public function cantidadEditar()
	{
			$datosCantidad=array(
        "id_cant"=>$this->input->post("id_cant"),
        "unitario_cant"=>$this->input->post("unitario_cant"),
        "paleta_cant"=>$this->input->post("paleta_cant")
			);
			$id=$this->input->post("id_cant");

			if($this->Cantidad->editar($id,$datosCantidad)) {
				$resultado=array("estado"=>"ok", "mensaje"=>"ingresado exitosamente");
			}else {
				$resultado=array("estado"=>"error");
		}
		redirect('cantidades/index');

		}


}//no eliminar esta llave
