<div class="container">
  <legend class="text-center">
    <i class="glyphicon glyphicon-plus-sign "></i><br>
    <b>Nueva Produccion</b>
  </legend>
  <hr>
  <br>
  <form class="" action="<?php echo site_url("producciones/guardarProduccion"); ?>" method="post" >
    <b>Cantidad:</b>
    <br>
    <input type="number" id="cantidad_pro" name="cantidad_pro" value="" placeholder="ingrese el nombre del producto" class="form-control">
    <br>
    <b>Requerido:</b>
    <br>
    <select id="requerido_pro" name="requerido_pro" class="form-control" required data-live-search="true">
      <option value="">seleccione</option>
      <option value="si">Si</option>
      <option value="no">No</option>
    </select>
     <br>
     <br>
     <b>Seleccione el producto:</b>
     <br>
      <br>
    <select class="form-control" name="fk_id_pro_ac" id="fk_id_pro_ac" required data-live-search="true">
        <option value="">--Seleccione el Producto--</option>
        <?php if ($listadoProducto): ?>
          <?php foreach ($listadoProducto->result() as $producto): ?>
              <option value="<?php echo $producto->id_produc; ?>">
                <?php echo $producto->nombre_produc; ?>
                |
                <?php echo $producto->tamaño_produc; ?>
              </option>
          <?php endforeach; ?>
        <?php else: ?>
          <h3><b>No existe producto</b></h3>
        <?php endif; ?>
    </select>
    <br>


    <br>
    <b>Fecha:</b>
    <input class="form-control"
    type="date" name="mes_pro" id="mes_pro"
    value="<?php echo date('Y-m-d'); ?>">
    <br>
    <button type="submit" name="button" class="btn btn-success">
      Guardar
    </button>
    <a href="<?php echo site_url('producciones/index'); ?>"
      class="btn btn-danger">
      Cancelar
    </a>
  </form>

</div>
<script type="text/javascript">
  $('#requerido_pro').selectpicker();
  $('#fk_id_pro_ac').selectpicker();
</script>
