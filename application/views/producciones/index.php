<script type="text/javascript">
  $("#menu-producciones").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<center class="text-info bg-dark">
  <div class="bg-success p-2 text-white"><h1 >Listado de producciones base de datos</h1></div>
  <br>
  <a href="<?php echo site_url("producciones/nuevo"); ?>" class="btn btn-primary  glyphicon glyphicon-pencil">
  agregar produccion </a>
</center>
<table class="table table-bordered
table-striped table-hover">
  <thead class="bg-success p-2 text-white">
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">Cantidad</th>
      <th class="text-center">Mes</th>
      <th class="text-center">Requerido</th>
      <th class="text-center">Nombre del producto</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
      <?php if ($listadoProducciones): ?>
          <?php foreach ($listadoProducciones->result() as $temporal): ?>
            <tr>
                <td class="text-center">
                  <?php echo $temporal->id_pro; ?>
                </td>
                <td class="text-center">
                  <?php echo $temporal->cantidad_pro; ?>
                </td>
                <td class="text-center">
                  <?php echo $temporal->mes_pro; ?>
                </td>
                <td class="text-center">
                  <?php echo $temporal->requerido_pro; ?>
                </td>
                <td class="text-center">
                  <?php echo $temporal->nombre_produc; ?>
                </td>
                <td class="text-center">
                <a href="<?php echo site_url("producciones/editar"); ?>/<?php echo $temporal->id_pro; ?>" class="btn btn-warning ">
                  <i class="glyphicon glyphicon-pencil">Editar</i>
                  </a>
                  <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
                  <a href="<?php echo site_url('producciones/borrar');?>/<?php echo $temporal->id_pro;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
                  <?php endif ?>
                </td>

            </tr>
          <?php endforeach; ?>
      <?php endif; ?>
  </tbody>
</table>
</div>
