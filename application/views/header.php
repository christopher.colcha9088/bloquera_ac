
<?php if(!$this->session->userdata('conectad0')): ?>
  <script type="text/javascript">
    window.location.href="<?php  echo site_url("seguridades/login");?>";
  </script>
<?php endif; ?>


<!DOCTYPE html>
<html lang=es dir="ltr">
  <head>
    <meta charset="utf-8">
    <title> bloquera ac</title>

    <!-- importacion de jquerry -->
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
  	<!-- importacion de bootstrap -->
  	<!-- Latest compiled and minified CSS -->
    <!--  Importacion de Jquerry Validate -->
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/jquery.validate.min.js'); ?>">
    </script>
        <!--  Importacion de Jquerry methods-->
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/additional-methods.min.js'); ?>">
    </script>
         <!--  Importacion de Jquerry español -->
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/messages_es_PE.min.js'); ?>">
    </script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>

    <!-- importacion sweetalert2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css" integrity="sha512-JzSVRb7c802/njMbV97pjo1wuJAE/6v9CvthGTDxiaZij/TFpPQmQPTcdXyUVucsvLtJBT6YwRb5LhVxX3pQHQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js" integrity="sha512-9V+5wAdU/RmYn1TP+MbEp5Qy9sCDYmvD2/Ub8sZAoWE2o6QTLsKx/gigfub/DlOKAByfhfxG5VKSXtDlWTcBWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


    <!-- Importacion Bootstrap id_pro -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js" integrity="sha512-yDlE7vpGDP7o2eftkCiPZ+yuUyEcaBwoJoIhdXv71KZWugFqEphIS3PU60lEkFaz8RxaVsMpSvQxMBaKVwA5xg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css" integrity="sha512-ARJR74swou2y0Q2V9k0GbzQ/5vJ2RBSoCWokg4zkfM29Fb3vZEQyv0iWBMW/yvKgyHSR/7D64pFMmU8nYmbRkg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/i18n/defaults-es_ES.min.js" integrity="sha512-RN/dgJo36dNkKVnb1XGzePP4/8XGa/r+On4XYUy8I1C5z+9SsIEU2rFh6TrunAnddKwtXwMdI0Se8HZxd0GtiQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <?php if ($this->session->flashdata('confirmacion')): ?>
          <script type="text/javascript">
            $(document).ready(function(){
              Swal.fire(
                'CONFIRMACIÓN', //titulo
                '<?php echo $this->session->flashdata('confirmacion'); ?>', //Contenido o mensaje
                'success' //Tipo de alerta
              )
            });
          </script>
        <?php endif; ?>


        <?php if ($this->session->flashdata('error')): ?>
          <script type="text/javascript">
            $(document).ready(function(){
              Swal.fire(
                'ERROR', //titulo
                '<?php echo $this->session->flashdata('error'); ?>', //Contenido o mensaje
                'error' //Tipo de alerta
              )
            });
          </script>
        <?php endif; ?>

  </head>
<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        </button>
        <a class="navbar-brand" href="#">BLOQUERA AC</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">

          <li id="menu-tienda"><a href="<?php echo site_url("tiendas/index"); ?>">Ventas</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Empleados <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li id="menu-trabajadores"><a href="<?php echo site_url("trabajadores/index"); ?>"> Trabajadores</a></li>
              <li id="menu-vendedor"><a href="<?php echo site_url("vendedores/index"); ?>"> Vendedor </a></li>
                      </ul>


          </li>
          <li class="dropdown">
            <ul class="dropdown-menu">
              <li id="menu-producciones"><a href="<?php echo site_url("producciones/index"); ?>">Producciones</a></li>
              <li id="menu-productos"><a href="<?php echo site_url("productos/index"); ?>">Producto</a></li>
              <li id="menu-inventarios"><a href="<?php echo site_url("indventarios/index"); ?>">Inventar</a></li>
                      </ul>


            <li id="menu-sucursales"><a href="<?php echo site_url("sucursales/index"); ?>"> Sucursales </a></li>
            <li id="menu-cantidad"> <a href="<?php echo site_url("cantidades/index"); ?>"> Cantidad </a> </li>
            <li id="menu-factura"> <a href="<?php echo site_url("facturas/index"); ?>"> Factura </a> </li>
          </li>
        </ul>


        <ul class="nav navbar-nav">

          <li id="menu-tipo"><a href="<?php echo site_url("tipos/index"); ?>"> Tipo </a></li>
          <li id="menu-configuracion"><a href="<?php echo site_url("configuraciones/index"); ?>"> Configuraciones </a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Productos <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li id="menu-producciones"><a href="<?php echo site_url("producciones/index"); ?>">Producciones</a></li>
              <li id="menu-productos"><a href="<?php echo site_url("productos/index"); ?>">Producto</a></li>
              <li id="menu-inventarios"><a href="<?php echo site_url("inventarios/index"); ?>">Inventario</a></li>
            </ul>


          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
