<script type="text/javascript">
  $("#menu-cantidades").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<legend class="text-center">
<img src="<?php echo base_url() ?>/assets/imagenes/logo1.jpg" alt="Logo" width="90" height="75" class="img-circle">
<center class="text-info bg-dark" > <div class="bg-success p-2 text-white"><h3> Gestion de cantidad</h3></div> </center>
</legend>

<?php if ($listadoCantidad): ?>
  <table id="tbl-cantidad" class="table table-stripe table-border table-hover">
<thead class="bg-success p-2 text-white">
  <th class="text-info bg-dark">Id</th>
  <th class="text-info bg-dark">Unitario</th>
  <th class="text-info bg-dark">Paleta</th>
  <th class="text-info bg-dark">Acciones</th>
</thead>
<tbody>
  <?php foreach ($listadoCantidad-> result() as $temporal): ?>
    <tr>
      <td class="text-center"> <?php echo $temporal->id_cant ;?></td>
      <td class="text-center"> <?php echo $temporal->unitario_cant ; ?></td>
      <td class="text-center"> <?php echo $temporal->paleta_cant ; ?></td>
      <td>
        <a href="<?php echo site_url("cantidades/editar"); ?>/<?php echo $temporal->id_cant; ?>" class="btn btn-warning" data-toggle="modal" data-target="#moda_editar"> <i class="glyphicon glyphicon-pencil" > Editar</i></a>
        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('cantidades/borrar');?>/<?php echo $temporal->id_cant;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>

      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>

<?php endif; ?>



<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<br>
<center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_cantidad">Nuevo Cantidad</button></center>

<!-- Modal -->
<div id="modal_cantidad" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nueva Cantidad</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_cantidad" class="" action="<?php echo site_url('cantidades/guardar'); ?>" method="post">
          <b>Unitario:</b>
          <br>
          <input type="number" id="unitario_cant" name="unitario_cant" value="" placeholder="ingrese su unitario" class="form-control">
          <br>
          <b>Paleta:</b>
          <br>
          <input type="number" id="paleta_cant" name="paleta_cant" value="" placeholder="ingrese su Paleta" class="form-control">
          <center>
            <button type="submit" name="button" class="btn btn-succes"> <i class="glyphicon glyphicon-ok"></i> Guardar </button>
          </center>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

</div>
</div>

<!-- Modal  editar -->
<div id="moda_editar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-edicion">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$("#frm_nuevo_cantidad").validate({
   rules:{
       unitario_cant:{
         required: true
       },
       paleta_cant:{
         required: true
       }
   },
   messages:{
     unitario_cant:{
       required: "Ingrese su unitario"
     },
     paleta_cant:{
       required: "Ingrese su paleta"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("cantidades/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_cantidad").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_cantidad").modal("hide");
       }
     });
   }
});
</script>

<script type="text/javascript">
  $('#tbl-cantidad').DataTable();
</script>

<script type="text/javascript">
    function cargarEdicion(id){
    //  alert("<?php echo site_url('Cantidades/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('cantidades/editar'); ?>/"+id);
      $("#modal_cantidad").modal("show");
    },

</script>
</div>
