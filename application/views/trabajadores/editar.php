<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<div class="row">
  <div class="col-md-12 text-center">

    <div class="bg-success p-2 text-white"><h3>Actualizar configuracion</h3></div>

  </div>
</div>
<div class="row">
  <div class="col-md-2">

  </div>
  <div class="col-md-7 text-center ">
    <?php if ($trabajador): ?>
      <form class="" id="frm_nueva_trabajor"  action="<?php echo site_url("trabajadores/trabajadorEditar"); ?>" method="post">
      <div class="bg-success p-2 text-white"><b>Nombre del trabajador</b></div>
      <br>
      <input type="text" id="nombre_trab" name="nombre_trab" value="<?php echo $trabajador->nombre_trab?>" placeholder="ingrese el nombre del trabajador" class="form-control">
      <br>
      <b>apellido:</b>
      <br>
      <input type="text" id="apellido_trab" name="apellido_trab" value="<?php echo $trabajador->apellido_trab?>" placeholder="ingrese el apellido del trabajador" class="form-control">
      <br>
      <b>Dias asistencia:</b>
      <br>
      <input type="date" id="dias_asis_trab" name="dias_asis_trab" value="<?php echo $trabajador->dias_asis_trab?>" placeholder="ingrese los dias de asistencia" class="form-control">
      <br>
      <b>Dias de inansistencia:</b>
      <br>
      <input type="date" id="dias_inasis_trab" name="dias_inasis_trab" value="<?php echo $trabajador->dias_inasis_trab?>" placeholder="ingrese los dias que a faltado" class="form-control">
      <br>
      <b>Sueldo del trabajador:</b>
      <br>
      <input type="number" id="sueldo_trab" name="sueldo_trab" value="<?php echo $trabajador->sueldo_trab?>" placeholder="ingrese el representante" class="form-control">
      <br>
      <b>Telefono:</b>
      <br>
      <input type="number" id="telefono_trab" name="telefono_trab" value="<?php echo $trabajador->telefono_trab?>" placeholder="ingrese el numero telefonico del trabajador" class="form-control">
      <br>
      <b>Cedula:</b>
      <br>
      <input type="number" id="cedula_trab" name="cedula_trab" value="<?php echo $trabajador->cedula_trab?>" placeholder="ingrese la cedula del trabajador" class="form-control">
      <br>
      <center>
        <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
        <button type="button" class="btn btn-warning " data-dismiss="modal">Close</button>
      </center>
    </form>
    <?php  else: ?>
    <div class="alert alert-danger">
      <b>No se encontro al trabajador</b>
    </div>
    <?php endif; ?>


    <div class="modal-footer">
    </div>


  </div>

    <div class="col-md-2">

    </div>

</div>
</div>
