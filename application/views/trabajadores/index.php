<script type="text/javascript">
  $("#menu-trabajadores").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<legend class="text-center">
  <div class="bg-success p-2 text-white"><b>Gestion de Trabajadores</b><hr></div>
</legend>

<?php if ($listadoTrabajador): ?>
  <table id="tbl-trabajador" class="table table-stripe table-border table-hover">
<thead class="bg-success p-2 text-white">
  <th class="text-center">Id</th>
  <th class="text-center">Nombre</th>
  <th class="text-center">apellido</th>
  <th class="text-center">Dias de asistencia</th>
  <th class="text-center">Dias de inansistencia</th>
  <th class="text-center">Sueldo</th>
  <th class="text-center">Telefono</th>
  <th class="text-center">Cedula</th>
  <th class="text-center">Acciones</th>
</thead>
<tbody>
  <?php foreach ($listadoTrabajador-> result() as $temporal): ?>
    <tr>
      <td class="text-center"><?php echo $temporal->id_trab; ?></td>
      <td class="text-center"><?php echo $temporal->nombre_trab; ?></td>
      <td class="text-center"><?php echo $temporal->apellido_trab; ?></td>
      <td class="text-center"><?php echo $temporal->dias_asis_trab; ?></td>
      <td class="text-center"><?php echo $temporal->dias_inasis_trab; ?></td>
      <td class="text-center"><?php echo $temporal->sueldo_trab; ?></td>
      <td class="text-center"><?php echo $temporal->telefono_trab	; ?></td>
      <td class="text-center"><?php echo $temporal->cedula_trab; ?></td>

      <td>
        <a href="<?php echo site_url("trabajadores/editar"); ?>/<?php echo $temporal->id_trab; ?>" class="btn btn-warning" data-toggle="modal" data-target="#moda_editar"> <i class="glyphicon glyphicon-pencil" > Editar</i></a>


        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('trabajadores/borrar');?>/<?php echo $temporal->id_trab;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>
      </td>
      </tr>
  <?php endforeach; ?>
</tbody>
  </table>
<?php else: ?>

<?php endif; ?>
<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_trabajador">Nuevo Trabajador</button>

<!-- Modal -->
<div id="modal_trabajador" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingrese un nuevo trabajador</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_trabajador" class="" action="<?php echo site_url('trabajadores/guardar'); ?>" method="post">
          <b>Nombre del trabajador</b>
          <br>
          <input type="text" id="nombre_trab" name="nombre_trab" value="" placeholder="ingrese el nombre del trabajador" class="form-control">
          <br>
          <b>apellido:</b>
          <br>
          <input type="text" id="apellido_trab" name="apellido_trab" value="" placeholder="ingrese el apellido del trabajador" class="form-control">
          <br>
          <b>Dias asistencia:</b>
          <br>
          <input type="date" id="dias_asis_trab" name="dias_asis_trab" value="" placeholder="ingrese los dias de asistencia" class="form-control">
          <br>
          <b>Dias de inansistencia:</b>
          <br>
          <input type="date" id="dias_inasis_trab" name="dias_inasis_trab" value="" placeholder="ingrese los dias que a faltado" class="form-control">
          <br>
          <b>Sueldo del trabajador:</b>
          <br>
          <input type="number" id="sueldo_trab" name="sueldo_trab" value="" placeholder="ingrese el representante" class="form-control">
          <br>
          <b>Telefono:</b>
          <br>
          <input type="number" id="telefono_trab" name="telefono_trab" value="" placeholder="ingrese el numero telefonico del trabajador" class="form-control">
          <br>
          <b>Cedula:</b>
          <br>
          <input type="number" id="cedula_trab" name="cedula_trab" value="" placeholder="ingrese la cedula del trabajador" class="form-control">
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
              <label for="">FOTOGRAFIA:</label>
            </div>
            <div class="col-md-7">
              <!-- image/* -->
              <input type="file"  id="foto_est" name="foto_est" value=""   required accept="image/*">
            </div>
          </div>
          <br>
          <center>
            <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
          </center>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal  editar -->
<div id="moda_editar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-edicion">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<script type="text/javascript">
  $('#tbl-trabajador').DataTable();
</script>

<script type="text/javascript">
    function cargarEdicion(id){
      $("#contenedor-edicion").load("<?php echo site_url('trabajadores/editar'); ?>/"+id);
      $("#modalEditarTrabajador").modal("show");
    }
</script>


<script type="text/javascript">
$("#frm_nuevo_trabajador").validate({
   rules:{
       nombre_trab:{
       minlength:3,
       maxlength:25,
         required:true
       },
       apellido_trab:{
       minlength:3,
       maxlength:25,
         required: true
       },
       dias_asis_trab:{
         required: true
       },
       dias_inasis_trab:{
         required: true
       },
       sueldo_trab:{
       minlength:2,
       maxlength:5,
         required: true
       },
       telefono_trab:{
       minlength:10,
       maxlength:10,
         required: true
       },
       cedula_trab:{
       minlength:10,
       maxlength:10,
         required: true
       }
   },
   messages:{
     nombre_trab:{
       required:"ingrese el nombre del trabajador"
     },
     apellido_trab:{
       required: "ingrese el apellido del trabajador"
     },
     dias_asis_trab:{
       required: "ingrese los dias de asistencia"
     },
     dias_inasis_trab:{
       required: "ingrese los dias de inansistencia"
     },
     sueldo_trab:{
       required: "ingrese el sueldo del trabajador"
     },
     telefono_trab:{
       required: "ingrese el telefono del trabajador"
     },
     cedula_trab:{
       required: "ingrese la cedula del trabajador"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("trabajadores/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_trabajador").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_trabajador").modal("hide");
       }
     });
   }
   });
   </script>
</div>
