<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<div class="row">
  <div class="col-md-12 text-center">

    <div class="bg-success p-2 text-white"><h3>Actualizar Strock</h3></div>

  </div>
</div>
<div class="row">
  <div class="col-md-2">

  </div>
  <div class="col-md-7 text-center ">
    <?php if ($inventario): ?>
      <form class="" id="frm_nueva_configuracion"  action="<?php echo site_url("inventarios/inventarioEditar"); ?>" method="post">
      <center>
      <input type="hidden" name="id_inv" value="<?php echo $inventario->id_inv?>">
      </center>
      <b>Arena:</b>
      <br>
      <input type="number" id="arena_inv" name="arena_inv" value="<?php echo $inventario->arena_inv?>" placeholder="ingrese la cantidad de arena" class="form-control">
      <br>
      <b>Piedra:</b>
      <br>
      <input type="number" id="piedra_inv" name="piedra_inv" value="<?php echo $inventario->piedra_inv?>" placeholder="ingrese la cantidad de piedra" class="form-control">
      <br>
      <b>Agua:</b>
      <br>
      <input type="number" id="agua_inv" name="agua_inv" value="<?php echo $inventario->agua_inv?>" placeholder="ingrese la cantidad de agua" class="form-control">
      <br>
      <center>
        <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
        <button type="button" class="btn btn-warning " data-dismiss="modal">Close</button>
      </center>
    </form>
    <?php  else: ?>
    <div class="alert alert-danger">
      <b>No se encontro al estudiante</b>
    </div>
    <?php endif; ?>


    <div class="modal-footer">
    </div>


  </div>

    <div class="col-md-2">

    </div>

</div>
</div>
