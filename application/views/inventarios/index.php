<script type="text/javascript">
  $("#menu-inventario").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<legend class="bg-success p-2 text-white">
  <img src="<?php echo base_url() ?>/assets/imagenes/logo1.jpg" alt="Logo" width="90" height="75" class="img-circle">
  <div class="bg-success p-2 text-white"><b>Gestionar el Inventario</b><hr></div>
</legend>


<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_inventario">Nuevo Inventario</button>

<!-- Modal -->
<div id="modal_inventario" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nuevo Inventario</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_inventario" class="" action="<?php echo site_url('inventarios/guardar'); ?>" method="post">
          <b>Arena:</b>
          <br>
          <input type="number" id="arena_inv" name="arena_inv" value="" placeholder="ingrese la cantidad de arena" class="form-control">
          <br>
          <b>Piedra:</b>
          <br>
          <input type="number" id="piedra_inv" name="piedra_inv" value="" placeholder="ingrese la cantidad de piedra" class="form-control">
          <br>
          <b>Agua:</b>
          <br>
          <input type="number" id="agua_inv" name="agua_inv" value="" placeholder="ingrese la cantidad de agua" class="form-control">
          <br>
          <center>
            <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
          </center>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal  editar -->
<div id="moda_editar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-edicion">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="inventario" class="text-center" >
  <center><i class="fa fa-spinner fa-spin fa-6x"></i></center>

</div>
<script type="text/javascript">
  function consulta()
  {
      $("#inventario").html('<center><i class="fa fa-spinner fa-spin fa-6x"></i></center>');$("#inventario").load("<?php echo site_url('inventarios/listado'); ?>");
  }
  consulta();
</script>






<script type="text/javascript">
    function cargarEdicion(id){
      $("#contenedor-edicion").load("<?php echo site_url('inventarios/editar'); ?>/"+id);
      $("#modalEditarInventario").modal("show");
    }
</script>


<script type="text/javascript">
$("#frm_nuevo_inventario").validate({
   rules:{
       arena_inv:{
       minlength:1,
       maxlength:3,
         required:true
       },
       piedra_inv:{
       minlength:1,
       maxlength:3,
         required: true
       },
       agua_inv:{
       minlength:1,
       maxlength:3,
         required: true
       }
   },
   messages:{
     arena_inv:{
       required:"ingrese el nombre de la emoresa"
     },
     piedra_inv:{
       required: "ingrese el ruc"
     },
     agua_inv:{
       required: "ingrese el telefono"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("inventarios/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_inventario").modal("hide");
         consulta();
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
       }
     });
   }
});
</script>

</div>
