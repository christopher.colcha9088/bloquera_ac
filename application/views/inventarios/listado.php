<?php if ($Inventario): ?>
  <table id="tbl-inventario" class="table table-stripe table-border table-hover">
<thead class="bg-success p-2 text-white">
  <th class="text-center">Id</th>
  <th class="text-center">Arena</th>
  <th class="text-center">Piedra</th>
  <th class="text-center">Agua</th>
  <th class="text-center">Acciones</th>
</thead>
<tbody>
  <?php foreach ($Inventario-> result() as $temporal): ?>
    <tr>
      <td class="text-center"><?php echo $temporal->id_inv; ?></td>
      <td class="text-center"><?php echo $temporal->arena_inv; ?></td>
      <td class="text-center"><?php echo $temporal->piedra_inv; ?></td>
      <td class="text-center"><?php echo $temporal->agua_inv; ?></td>

      <td>
        <a href="<?php echo site_url("inventarios/editar"); ?>/<?php echo $temporal->id_inv; ?>" class="btn btn-warning" data-toggle="modal" data-target="#moda_editar"> <i class="glyphicon glyphicon-pencil" > Editar</i></a>
        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('inventarios/borrar');?>/<?php echo $temporal->id_inv;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>

      </td>
      </tr>
  <?php endforeach; ?>
</tbody>
  </table>
<?php else: ?>

<?php endif; ?>
<script type="text/javascript">
  $('#tbl-inventario').DataTable();
</script>
<script type="text/javascript">
    function cargarEdicion(id){
      $("#contenedor-edicion").load("<?php echo site_url('tiendas/editar'); ?>/"+id);
      $("#modalEditarTienda").modal("show");

    }
</script>
