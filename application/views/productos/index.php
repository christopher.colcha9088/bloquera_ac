<script type="text/javascript">
  $("#menu-productos").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">
<legend class="text-info bg-dark" >

<center> <div class="bg-success p-2 text-white"><h1><b>gestion de producto</b><hr></h1></div> </center>
</legend>
<?php if ($listadoProducto): ?>
  <table id="tbl-producto" class="table table-stripe table-border table-hover">
<thead class="bg-success p-2 text-white">
  <th class="text-center">Id</th>
  <th class="text-center">Nombre</th>
  <th class="text-center">Cantidad</th>
  <th class="text-center">Tamaño</th>
  <th class="text-center">Acciones</th>
</thead>
<tbody>
  <?php foreach ($listadoProducto-> result() as $temporal): ?>
    <tr>
      <td class="text-center"><?php echo $temporal->id_produc; ?></td>
      <td class="text-center"><?php echo $temporal->nombre_produc; ?></td>
      <td class="text-center"><?php echo $temporal->cantidad_produc; ?></td>
      <td class="text-center"><?php echo $temporal->tamaño_produc; ?></td>
      <td>
        <a href="<?php echo site_url("productos/editar"); ?>/<?php echo $temporal->id_produc; ?>" class="btn btn-warning"> <i class="glyphicon glyphicon-pencil"> Editar</i></a>

        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('productos/borrar');?>/<?php echo $temporal->id_produc;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>
      </td>
      </tr>
  <?php endforeach; ?>
</tbody>
  </table>
<?php else: ?>

<?php endif; ?>
<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_producto">Nuevo Producto</button>

<!-- Modal -->
<div id="modal_producto" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nuevo Producto</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_producto" class="" action="<?php echo site_url('productos/guardar'); ?>" method="post">
          <b>Nombre:</b>
          <br>
          <input type="text" id="nombre_produc" name="nombre_produc" value="" placeholder="ingrese el nombre del producto" class="form-control">
          <br>

          <b>Cantidad:</b>
          <br>
          <input type="number" id="cantidad_produc" name="cantidad_produc" value="" placeholder="ingrese la cantidad" class="form-control">
          <br>

          <b>Tamaño:</b>
          <br>
          <input type="number" id="tamaño_produc" name="tamaño_produc" value="" placeholder="ingrese el tamaño" class="form-control">
          <br>
          <center>
            <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
          </center>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$("#frm_nuevo_producto").validate({
   rules:{
       nombre_produc:{
       minlength:3,
         required:true
       },
       cantidad_produc:{
         required: true
       },
       tamaño_produc:{
         required: true
       }
   },
   messages:{
     nombre_produc:{
       required:"ingrese un producto"
     },
     cantidad_produc:{
       required: "ingrese una cantidad"
     },
     tamaño_produc:{
       required: "ingrese un tamaño"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("productos/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_producto").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_producto").modal("hide");
       }
     });
   }
});
</script>

<script type="text/javascript">
  $('#tbl-producto').DataTable();
</script>
</div>
<!-- Jumbotron -->
