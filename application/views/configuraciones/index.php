<script type="text/javascript">
  $("#menu-configuracion").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<legend class="bg-success p-2 text-white">
  <img src="<?php echo base_url() ?>/assets/imagenes/logo1.jpg" alt="Logo" width="90" height="75" class="img-circle">
  <b>gestion de Configuracion</b><hr>
</legend>

<?php if ($listadoConfiguracion): ?>
  <table id="tbl-configuracion" class="table table-stripe table-border table-hover">
<thead class="bg-success p-2 text-white">
  <th class="text-center">Id</th>
  <th class="text-center">Nombre de la empresa</th>
  <th class="text-center">Ruc</th>
  <th class="text-center">Telefono</th>
  <th class="text-center">Direccion</th>
  <th class="text-center">Representante</th>
  <th class="text-center">Acciones</th>
</thead>
<tbody>
  <?php foreach ($listadoConfiguracion-> result() as $temporal): ?>
    <tr>
      <td class="text-center"><?php echo $temporal->id_con_ac; ?></td>
      <td class="text-center"><?php echo $temporal->nombre_empresa_ac; ?></td>
      <td class="text-center"><?php echo $temporal->ruc_ac; ?></td>
      <td class="text-center"><?php echo $temporal->telefono_ac; ?></td>
      <td class="text-center"><?php echo $temporal->dirreccion_ac; ?></td>
      <td class="text-center"><?php echo $temporal->representante_ac; ?></td>

      <td>
        <a href="<?php echo site_url("configuraciones/editar"); ?>/<?php echo $temporal->id_con_ac; ?>" class="btn btn-warning" data-toggle="modal" data-target="#moda_editar"> <i class="glyphicon glyphicon-pencil" > Editar</i></a>

        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('configuraciones/borrar');?>/<?php echo $temporal->id_con_ac;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>
      </td>
      </tr>
  <?php endforeach; ?>
</tbody>
  </table>
<?php else: ?>

<?php endif; ?>
<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_configuracion">Nuevo Configuracion</button>

<!-- Modal -->
<div id="modal_configuracion" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nuevo Configuracion</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_configuracion" class="" action="<?php echo site_url('configuraciones/guardar'); ?>" method="post">
          <b>Nombre de Empresa:</b>
          <br>
          <input type="text" id="nombre_empresa_ac" name="nombre_empresa_ac" value="" placeholder="ingrese el nombre del configuracion" class="form-control">
          <br>
          <b>Ruc:</b>
          <br>
          <input type="number" id="ruc_ac" name="ruc_ac" value="" placeholder="ingrese el ruc" class="form-control">
          <br>
          <b>telefono:</b>
          <br>
          <input type="number" id="telefono_ac" name="telefono_ac" value="" placeholder="ingrese el telefono" class="form-control">
          <br>
          <b>Direccion:</b>
          <br>
          <input type="text" id="dirreccion_ac" name="dirreccion_ac" value="" placeholder="ingrese la direccion" class="form-control">

          <br>
          <b>Representante:</b>
          <br>
          <input type="text" id="representante_ac" name="representante_ac" value="" placeholder="ingrese el representante" class="form-control">
          <br>
          <center>
            <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
          </center>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal  editar -->
<div id="moda_editar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-edicion">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<script type="text/javascript">
  $('#tbl-configuracion').DataTable();
</script>

<script type="text/javascript">
    function cargarEdicion(id){
    //  alert("<?php echo site_url('configuraciones/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('configuraciones/editar'); ?>/"+id);
      $("#modalEditarConfiguracion").modal("show");
    }
</script>


<script type="text/javascript">
$("#frm_nuevo_configuracion").validate({
   rules:{
       nombre_empresa_ac:{
       minlength:3,
         required:true
       },
       ruc_ac:{
       minlength:9,
       maxlength:9,
         required: true
       },
       telefono_ac:{
       minlength:10,
       maxlength:10,
         required: true
       },
       dirreccion_ac:{
         required: true
       },
       representante_ac:{
         required: true
       }
   },
   messages:{
     nombre_empresa_ac:{
       required:"ingrese el nombre de la emoresa"
     },
     ruc_ac:{
       required: "ingrese el ruc"
     },
     telefono_ac:{
       required: "ingrese el telefono"
     },
     dirreccion_ac:{
       required: "ingrese una direccion"
     },
     representante_ac:{
       required: "ingrese un represntante"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("configuraciones/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_configuracion").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_configuracion").modal("hide");
       }
     });
   }
});
</script>
</div>
