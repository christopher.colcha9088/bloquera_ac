<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<div class="row">
  <div class="col-md-12 text-center">


<div class="bg-success p-2 text-white"><h3>Actualizar configuracion</h3>

</div>

  </div>
</div>
<div class="row">
  <div class="col-md-2">

  </div>
  <div class="col-md-7 text-center ">
    <?php if ($configuracion): ?>
      <form class="" id="frm_nueva_configuracion"  action="<?php echo site_url("configuraciones/configuracionEditar"); ?>" method="post">
      <center>
      <input type="hidden" name="id_con_ac" value="<?php echo $configuracion->id_con_ac?>">
      </center>
      <b>Nombre de Empresa:</b>
      <br>
      <input type="text" id="nombre_empresa_ac" name="nombre_empresa_ac" value="<?php echo $configuracion->nombre_empresa_ac?>" placeholder="ingrese el nombre del configuracion" class="form-control">
      <br>
      <b>Ruc:</b>
      <br>
      <input type="number" id="ruc_ac" name="ruc_ac" value="<?php echo $configuracion->ruc_ac?>" placeholder="ingrese el ruc" class="form-control">
      <br>
      <b>telefono:</b>
      <br>
      <input type="number" id="telefono_ac" name="telefono_ac" value="<?php echo $configuracion->telefono_ac?>" placeholder="ingrese el telefono" class="form-control">
      <br>
      <b>Direccion:</b>
      <br>
      <input type="text" id="dirreccion_ac" name="dirreccion_ac" value="<?php echo $configuracion->dirreccion_ac?>" placeholder="ingrese la direccion" class="form-control">
      <br>
      <b>Representante:</b>
      <br>
      <input type="text" id="representante_ac" name="representante_ac" value="<?php echo $configuracion->representante_ac?>" placeholder="ingrese el representante" class="form-control">
      <br>
      <center>
        <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
        <button type="button" class="btn btn-warning " data-dismiss="modal">Close</button>
      </center>
    </form>
    <?php  else: ?>
    <div class="alert alert-danger">
      <b>No se encontro al estudiante</b>
    </div>
    <?php endif; ?>


    <div class="modal-footer">
    </div>


  </div>

    <div class="col-md-2">

    </div>

</div>

</div>
