<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">
<div class="row">
  <div class="col-md-12 text-center">

    <div class="bg-success p-2 text-white"><h3>Actualizar Nueva vendedor</h3></div>

  </div>
</div>
<div class="row">
  <div class="col-md-2">
  </div>
  <div class="col-md-7 text-center ">
    <?php if ($vendedor): ?>
    <form id="frm_nuevo_vendedor" class="" action="<?php echo site_url('vendedores/vendedorEditar'); ?>" method="post">
      <center>
        <input type="hidden" name="id_ven" value="<?php echo $vendedor->id_ven?>">
      </center>
    <label for=""> Nombre: </label><br>
    <input type="text" name="nombre_ven"
    value="<?php echo $vendedor->nombre_ven; ?>"
    id="nombre_ven" class="form-control"> <br>

    <label for=""> Apellido: </label><br>
    <input type="text" name="apellido_ven"
    value="<?php echo $vendedor->apellido_ven; ?>"
    id="apellido_ven" class="form-control"> <br>

    <center>
      <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar </button>
      <button type="button" class="btn btn-warning " data-dismiss="modal"> Close </button>
    </center>
</form>
<?php  else: ?>
<div class="alert alert-danger">
  <b>No se encontro al Vendedor</b>
</div>
<?php endif; ?>


<div class="modal-footer">


</div>


</div>

<div class="col-md-2">

</div>

</div>
</div>
