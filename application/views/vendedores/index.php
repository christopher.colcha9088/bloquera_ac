<script type="text/javascript">
  $("#menu-vendedores").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<legend class="text-center">
<img src="<?php echo base_url() ?>/assets/imagenes/logo1.jpg" alt="Logo" width="90" height="75" class="img-circle">
<center class="bg-success p-2 text-white">  <h3> Gestion de Vendedor</h3> </center>
</legend>

<?php if ($listadovendedores): ?>
  <table id="tbl-vendedor" class="table table-stripe table-border table-hover">
<thead class="bg-success p-2 text-white">
  <th class="text-center">Id</th>
  <th class="text-center">Nombre</th>
  <th class="text-center">Apellido</th>
  <th class="text-center">Acciones</th>
</thead>
<tbody>
  <?php foreach ($listadovendedores-> result() as $temporal): ?>
    <tr>
      <td class="text-center"> <?php echo $temporal->id_ven ;?></td>
      <td class="text-center"> <?php echo $temporal->nombre_ven ; ?></td>
      <td class="text-center"> <?php echo $temporal->apellido_ven ; ?></td>
      <td>
        <a href="<?php echo site_url("vendedores/editar"); ?>/<?php echo $temporal->id_ven; ?>" class="btn btn-warning" data-toggle="modal" data-target="#moda_editar"> <i class="glyphicon glyphicon-pencil" > Editar</i></a>
        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('vendedores/borrar');?>/<?php echo $temporal->id_ven;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>


      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>

<?php endif; ?>



<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<br>
<center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_vendedor">Nuevo vendedor</button></center>

<!-- Modal -->
<div id="modal_vendedor" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nueva Vendedor</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_vendedor" class="" action="<?php echo site_url('vededores/guardar'); ?>" method="post">
          <b>Nombre:</b>
          <br>
          <input type="text" id="nombre_ven" name="nombre_ven" value="" placeholder="ingrese su nombre" class="form-control">
          <br>
          <b>Apellido:</b>
          <br>
          <input type="text" id="apellido_ven" name="apellido_ven" value="" placeholder="ingrese su apellido" class="form-control">
          <center>
            <button type="submit" name="button" class="btn btn-succes"> <i class="glyphicon glyphicon-ok"></i> Guardar </button>
          </center>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

</div>
</div>

<!-- Modal  editar -->
<div id="moda_editar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-edicion">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$("#frm_nuevo_vendedor").validate({
   rules:{
       nombre_ven:{
       minlength:10,
       maxlength:10,
         required: true
       },
       apellido_ven:{
       minlength:10,
       maxlength:10,
         required: true
       }
   },
   messages:{
     nombre_ven:{
       required: "Ingrese su unitario"
     },
     apellido_ven:{
       required: "Ingrese su paleta"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("vendedores/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_vendedor").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_vendedor").modal("hide");
       }
     });
   }
});
</script>

<script type="text/javascript">
  $('#tbl-vendedor').DataTable();
</script>

<script type="text/javascript">
    function cargarEdicion(id){
    //  alert("<?php echo site_url('vendedores/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('vendedores/editar'); ?>/"+id);
      $("#modal_vendedor").modal("show");
    },

</script>
</div>
