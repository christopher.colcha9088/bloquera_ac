<script type="text/javascript">
  $("#menu-tienda").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<center> <div class="bg-success p-2 text-white"><h1><b>Gestion de ventas </b><hr></h1></div> </center>


<?php if ($listadoTienda): ?>
<table id="tbl-tienda" class="table table-striped table-bordered table-hover">
<thead class="bg-success p-2 text-white">
  <div class="p-3 mb-2 bg-dark text-white">
  <th class="text-center">Id</th>
  <th class="text-center">Nombre del vendedor </th>
  <th class="text-center">Nombre del producto</th>
  <th class="text-center">Fecha</th>
  <th class="text-center">Acciones</th>
</thead>


<tbody>
  <?php foreach ($listadoTienda-> result() as $temporal): ?>
    <tr>
      <td class="text-center"><?php echo $temporal->id_tie; ?></td>
      <td class="text-center">
        <?php echo $temporal->nombre_ven; ?>
        <?php echo $temporal->apellido_ven; ?>
      </td>
      <td class="text-center">
        <?php echo $temporal->nombre_produc; ?>
        <?php echo $temporal->cantidad_produc; ?>
      </td>
      <td class="text-center"><?php echo $temporal->fecha_tie; ?></td>
      <td>
        <a href="<?php echo site_url("tiendas/editar"); ?>/<?php echo $temporal->id_tie; ?>" class="btn btn-warning" data-toggle="modal" data-target="#moda_editar"> <i class="glyphicon glyphicon-pencil" > Editar</i></a>

        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('tiendas/borrar');?>/<?php echo $temporal->id_tie;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>
      </td>
      </tr>
  <?php endforeach; ?>
</tbody>
  </table>
<?php else: ?>

<?php endif; ?>
<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_tienda">Nuevo Venta</button>

<!-- Modal -->
<div id="modal_tienda" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nueva Venta</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_tienda" class="" action="<?php echo site_url('tiendas/guardar'); ?>" method="post">
          <b>Producto:</b>
          <br>
          <select class="form-control" name="fk_id_produc" id="fk_id_produc" required data-live-search="true">
              <option value="">--Seleccione el Producto--</option>
              <?php if ($listadoProducto): ?>
                <?php foreach ($listadoProducto->result() as $producto): ?>
                    <option value="<?php echo $producto->id_produc; ?>">
                      <?php echo $producto->nombre_produc; ?>
                      |
                      <?php echo $producto->cantidad_produc; ?>
                    </option>
                <?php endforeach; ?>
              <?php else: ?>
                <h3><b>No existe producto</b></h3>
              <?php endif; ?>
          </select>
          <br>
          <b>Vendedores:</b>
          <br>
          <select class="form-control" name="fk_id_ven" id="fk_id_ven" required data-live-search="true">
              <option value="">--Seleccione al Vendedor--</option>
              <?php if ($listadoVendedor): ?>
                <?php foreach ($listadoVendedor->result() as $vendedor): ?>
                    <option value="<?php echo $vendedor->id_ven; ?>">
                      <?php echo $vendedor->nombre_ven; ?>
                      |
                      <?php echo $vendedor->apellido_ven; ?>
                    </option>
                <?php endforeach; ?>
              <?php else: ?>
                <h3><b>No existe vendedor</b></h3>
              <?php endif; ?>
          </select>
          <br>

          <b>Fecha:</b>
          <input class="form-control"
          type="date" name="fecha_tie" id="fecha_tie"
          value="<?php echo date('Y-m-d'); ?>">
          <br>
          <center>
            <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
          </center>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<!-- Modal  editar -->
<div id="moda_editar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-edicion">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<script type="text/javascript">
  $('#tbl-tienda').DataTable();
</script>

<script type="text/javascript">
    function cargarEdicion(id){
      $("#contenedor-edicion").load("<?php echo site_url('tiendas/editar'); ?>/"+id);
      $("#modalEditarTienda").modal("show");

    }
</script>

<script type="text/javascript">
  $('#fk_id_ven').selectpicker();
  $('#fk_id_produc').selectpicker();
</script>

<script type="text/javascript">
$("#frm_nuevo_tienda").validate({
   rules:{
       fk_id_produc:{
         required:true
       },
       fk_id_ven:{
         required:true
       },
       fecha_tie:{
         required: true
       }
   },
   messages:{
     fk_id_produc:{
       required:"ingrese el nombre del producto"
     },
       fk_id_ven:{
         required:"ingrese el nombre del vendedor"
       },
     fecha_tie:{
       required: "ingrese fecha"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("tiendas/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_tienda").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_tienda").modal("hide");
       }
     });
   }
});
</script>
</div>
