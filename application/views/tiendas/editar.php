<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<div class="row">
  <div class="col-md-12 text-center">

    <div class="bg-success p-2 text-white"><h3>Actualizar nuestra Venta</h3></div>


  </div>
</div>
<div class="row">
  <div class="col-md-2">

  </div>
  <div class="col-md-7 text-center ">
    <?php if ($tienda): ?>
      <form class="" id="frm_editar_tienda"  action="<?php echo site_url("tiendas/tiendaEditar"); ?>" method="post">

      <center>
      <input type="hide" name="id_tie" value="<?php echo $tienda->id_tie?>" >
      </center>
      <b>Producto:</b>
      <br>
      <select class="form-control" name="fk_id_produc" id="fk_id_produc" required data-live-search="true">
          <option value="">--Seleccione el Producto--</option>
          <?php if ($producto): ?>
            <?php foreach ($producto->result() as $temporal): ?>
                <option value="<?php echo $temporal->id_produc; ?>">
                  <?php echo $temporal->nombre_produc; ?>
                  |
                  <?php echo $temporal->cantidad_produc; ?>
                </option>
            <?php endforeach; ?>
          <?php else: ?>
            <h3><b>No existe producto</b></h3>
          <?php endif; ?>
      </select>
      <br>
      <b>Vendedor:</b>
      <br>
      <select class="form-control" name="fk_id_ven" id="fk_id_ven" required data-live-search="true">
          <option value="">--Seleccione el Vendedor--</option>
          <?php if ($vendedor): ?>
            <?php foreach ($vendedor->result() as $temporal): ?>
                <option value="<?php echo $temporal->id_ven; ?>">
                  <?php echo $temporal->nombre_ven; ?>
                  |
                  <?php echo $temporal->apellido_ven; ?>
                </option>
            <?php endforeach; ?>
          <?php else: ?>
            <h3><b>No existe producto</b></h3>
          <?php endif; ?>
      </select>
      <br>
      <b>Fecha:</b>
      <input class="form-control" type="date" name="fecha_tie" id="fecha_tie" value="<?php echo $tienda->fecha_tie?>">
      <br>
      <center>
        <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
        <button type="button" class="btn btn-warning " data-dismiss="modal">Close</button>
      </center>
    </form>
    <?php  else: ?>
    <div class="alert alert-danger">
      <b>No se encontro centa</b>
    </div>
    <?php endif; ?>
    <div class="modal-footer">
    </div>
  </div>

    <div class="col-md-2">

    </div>

</div>



<script type="text/javascript">
   $('#fk_id_produc').val("<?php echo $tienda->fk_id_produc; ?>");
   $('#fk_id_ven').val("<?php echo $tienda->fk_id_ven; ?>");
   $('#fk_id_produc').selectpicker();
   $('#fk_id_ven').selectpicker();
</script>


<script type="text/javascript">
$("#frm_editar_tienda").validate({
   rules:{
       fk_id_produc:{
         required:true
       },
       fk_id_ven:{
         required:true
       },
       fecha_tie:{
         required: true
       }
   },
   messages:{
     fk_id_produc:{
       required:"ingrese el nombre del producto"
     },
       fk_id_ven:{
         required:"ingrese el nombre del vendedor"
       },
     fecha_tie:{
       required: "ingrese fecha"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("tiendas/tiendaEditar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'
         );

         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_tienda").modal("hide");
       }
     });
   }
});
</script>
</div>
