<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">


<div class="row">
  <div class="col-md-12 text-center">

    <div class="bg-success p-2 text-white"><h3>Actualizar Nueva Tipos</h3>

    </div>

  </div>
</div>
<div class="row">
  <div class="col-md-2">
  </div>
  <div class="col-md-7 text-center ">
    <?php if ($tipo): ?>
    <form id="frm_nuevo_tipo" class="" action="<?php echo site_url('tipos/tipoEditar'); ?>" method="post">
      <center>
        <input type="hidden" name="id_tip" value="<?php echo $tipo->id_tip?>">
      </center>
    <label for=""> Tipo: </label><br>
    <input type="text" name="tipo_tip"
    value="<?php echo $tipo->tipo_tip; ?>"
    id="tipo_tip" class="form-control"> <br>

    <label for=""> Medida: </label><br>
    <input type="text" name="medida_tip"
    value="<?php echo $tipo->medida_tip; ?>"
    id="medida_tip" class="form-control"> <br>

    <center>
      <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Editar </button>
      <button type="button" class="btn btn-warning " data-dismiss="modal"> Close </button>
    </center>
</form>
<?php  else: ?>
<div class="alert alert-danger">
  <b>No se encontro al tipo</b>
</div>
<?php endif; ?>


<div class="modal-footer">


</div>


</div>

<div class="col-md-2">

</div>

</div>

</div>
