
<script type="text/javascript">
  $("#menu-tipo").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">


<legend class="text-center">
<img src="<?php echo base_url() ?>/assets/imagenes/logo1.jpg" alt="Logo" width="90" height="75" class="img-circle">
<center class="text-info bg-dark"><div class="bg-success p-2 text-white"> <h3> Gestion de tipo</h3></div> </center>
</legend>

<?php if ($listadotipo): ?>
  <table id="tbl-tipo" class="table table-stripe table-border table-hover">
<thead class="bg-success p-2 text-white">
  <th class="text-info bg-dark">Id</th>
  <th class="text-info bg-dark">tipo</th>
  <th class="text-info bg-dark">medida</th>
  <th class="text-info bg-dark">Acciones</th>
</thead>
<tbody>
  <?php foreach ($listadotipo-> result() as $temporal): ?>
    <tr>
      <td class="text-center"> <?php echo $temporal->id_tip ; ?></td>
      <td class="text-center"> <?php echo $temporal->tipo_tip ; ?></td>
      <td class="text-center"> <?php echo $temporal->medida_tip ; ?></td>
      <td>
        <a href="<?php echo site_url("tipos/editar"); ?>/<?php echo $temporal->id_tip; ?>" class="btn btn-warning" data-toggle="modal" data-target="#moda_editar"> <i class="glyphicon glyphicon-pencil" > Editar</i></a>

        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('tipos/borrar');?>/<?php echo $temporal->id_tip;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>

<?php endif; ?>



<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<br>
<center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_tipo">Nuevo tipo</button></center>

<!-- Modal -->
<div id="modal_tipo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nueva tipo</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_tipo" class="" action="<?php echo site_url('tipos/guardar'); ?>" method="post">
          <b>Tipo:</b>
          <br>
          <input type="text" id="tipo_tip" name="tipo_tip" value="" placeholder="ingrese su tipo" class="form-control">
          <br>
          <b>Medida:</b>
          <br>
          <input type="number" id="medida_tip" name="medida_tip" value="" placeholder="ingrese su medidas" class="form-control">
          <center>
            <button type="submit" name="button" class="btn btn-succes"> <i class="glyphicon glyphicon-ok"></i> Guardar </button>
          </center>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

</div>
</div>

<!-- Modal  editar -->
<div id="moda_editar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-edicion">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$("#frm_nuevo_tipo").validate({
   rules:{
       tipo_tip:{
         required: true
       },
       medida_tip:{
       maxlength:10,
         required: true
       }
   },
   messages:{
     tipo_tip:{
       required: "Ingrese su tipo"
     },
     medida_tip:{
       required: "Ingrese su medida"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("tipos/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_tipo").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_tipo").modal("hide");
       }
     });
   }
});
</script>

<script type="text/javascript">
  $('#tbl-tipo').DataTable();
</script>

<script type="text/javascript">
    function cargarEdicion(id){
    //  alert("<?php echo site_url('tipos/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('tipos/editar'); ?>/"+id);
      $("#modal_tipo").modal("show");
    },

</script>
</div>
