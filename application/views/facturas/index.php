<script type="text/javascript">
  $("#menu-factura").addClass('active');
</script>

<legend class="text-center">
  <img src="<?php echo base_url() ?>/assets/imagenes/logo1.jpg" alt="Logo" width="90" height="75" class="img-circle">
  <b>Gestionar el Factura</b><hr>
</legend>


<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_factura">Nuevo Factura</button>


<div id="modal_factura" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nueva Venta</h4>
      </div>
<div class="modal-body">
  <form id="frm_nueva_factura" class="" action="<?php echo site_url('facturas/guardar'); ?>" method="post">
    <b>Nombre del cliente</b>
    <br>
    <input type="text" id="nombre_fac" name="nombre_fac" value="" placeholder="ingrese el nombre del cliente" class="form-control">
    <br>
    <b>direccion del cliente</b>
    <br>
    <input type="text" id="direccion_fac" name="direccion_fac" value="" placeholder="ingrese la direccion del cliente" class="form-control">
    <br>
    <b>cedula del cliente</b>
    <br>
    <input type="text" id="cedula_fac" name="cedula_fac" value="" placeholder="ingrese la cedula del cliente" class="form-control">
    <br>
    <b>Vendedores:</b>
    <br>
    <select class="form-control" name="fk_id_ven" id="fk_id_ven" required data-live-search="true">
        <option value="">--Seleccione al Vendedor--</option>
        <?php if ($listadoVendedor): ?>
          <?php foreach ($listadoVendedor->result() as $vendedor): ?>
              <option value="<?php echo $vendedor->id_ven; ?>">
                <?php echo $vendedor->nombre_ven; ?>
                |
                <?php echo $vendedor->apellido_ven; ?>
              </option>
          <?php endforeach; ?>
        <?php else: ?>
          <h3><b>No existe vendedor</b></h3>
        <?php endif; ?>
    </select>
    <br>
    <b>Producto:</b>
    <br>
    <select class="form-control" name="fk_id_produc" id="fk_id_produc" required data-live-search="true">
        <option value="">--Seleccione el Producto--</option>
        <?php if ($listadoProducto): ?>
          <?php foreach ($listadoProducto->result() as $producto): ?>
              <option value="<?php echo $producto->id_produc; ?>">
                <?php echo $producto->nombre_produc; ?>
                |
                <?php echo $producto->cantidad_produc; ?>
              </option>
          <?php endforeach; ?>
        <?php else: ?>
          <h3><b>No existe producto</b></h3>
        <?php endif; ?>
    </select>

    <br>
    <select class="form-control" name="fk_id_cant" id="fk_id_cant" required data-live-search="true">
        <option value="">--Seleccione el cantidad--</option>
        <?php if ($listadoCantidad): ?>
          <?php foreach ($listadoCantidad->result() as $cantidad): ?>
              <option value="<?php echo $cantidad->id_cant; ?>">
                <?php echo $cantidad->unitario_cant; ?>
                |
                <?php echo $cantidad->paleta_cant; ?>
              </option>
          <?php endforeach; ?>
        <?php else: ?>
          <h3><b>No existe producto</b></h3>
        <?php endif; ?>
    </select>
    <br>

    <b>Fecha:</b>
    <input class="form-control" type="date" name="fecha_fac" id="fecha_fac" value="<?php echo date('Y-m-d'); ?>">
    <br>
    <center>
      <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
    </center>
  </form>
</div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>
</div>
</div>
</div>



<!-- Modal  editar -->
<div id="moda_editar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-edicion">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="factura" class="text-center" >
  <center><i class="fa fa-spinner fa-spin fa-6x"></i></center>

</div>
<script type="text/javascript">
  function consulta()
  {
    $("#factura").html('<center><i class="fa fa-spinner fa-spin fa-6x"></i></center>');$("#factura").load("<?php echo site_url('facturas/listado'); ?>");
  }
  consulta();
</script>







<script type="text/javascript">
    function cargarEdicion(id){
      $("#contenedor-edicion").load("<?php echo site_url('facturas/editar'); ?>/"+id);
      $("#modalEditarFactura").modal("show");
    }
</script>


<script type="text/javascript">
$("#frm_nueva_factura").validate({
   rules:{
       nombre_fac:{
         minlength:3,
         required:true
       },
       direccion_fac:{
         required: true
       },
       cedula_fac:{
       minlength:10,
       maxlength:10,
         required: true
       },
       fecha_fac:{
         required:true
       },
       fk_id_produc:{
         required: true
       },
       fk_id_ven:{
         required: true
       },
       fk_id_cant:{
         required: true
         }
   },
   messages:{
     nombre_fac:{
       required:"ingrese el nombre del cliente"
     },
     direccion_fac:{
       required: "ingrese la direccion del cliente"
     },
     cedula_fac:{
       required: "ingrese la cedula"
     },
     fecha_fac:{
       required:"ingrese la fecha"
     },
     fk_id_produc:{
       required: "ingrese el producto"
     },
     fk_id_ven:{
       required: "ingrese al vendedor"
     },
     fk_id_cant:{
       required:"ingrese la cantidad"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("facturas/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_factura").modal("hide");
         consulta();
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
       }
     });
   }
});
</script>
