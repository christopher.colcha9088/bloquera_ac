<div class="row">
  <div class="col-md-12 text-center">

    <h3>Actualizar factura</h3>

  </div>
</div>
<div class="row">
  <div class="col-md-2">

  </div>
  <div class="col-md-7 text-center ">
    <?php if ($factura): ?>
      <form class="" id="frm_editar_factura"  action="<?php echo site_url("facturas/facturaEditar"); ?>" method="post">

      <center>
      <input type="hidden" name="id_fac" value="<?php echo $factura->id_fac?>" >
      </center>
      <b>Nombre del cliente:</b>
      <br>
      <input type="text" id="nombre_fac" name="nombre_fac" value="<?php echo $factura->nombre_fac?>" placeholder="ingrese el nombre del cliente por favor " class="form-control">
      <br>
      <b>direccion del cliente:</b>
      <br>
      <input type="text" id="direccion_fac" name="direccion_fac" value="<?php echo $factura->direccion_fac ?>" placeholder="ingrese la direccion del cliente por favor" class="form-control">
      <br>
      <b>cedula del cliente:</b>
      <br>
      <input type="text" id="cedula_fac" name="cedula_fac" value="<?php echo $factura->cedula_fac ?>" placeholder="ingrese la cedula del cliente" class="form-control">
      <br>
      <b>Producto:</b>
      <br>
      <select class="form-control" name="fk_id_produc" id="fk_id_produc" required data-live-search="true">
          <option value="">--Seleccione el Producto--</option>
          <?php if ($producto): ?>
            <?php foreach ($producto->result() as $temporal): ?>
                <option value="<?php echo $temporal->id_produc; ?>">
                  <?php echo $temporal->nombre_produc; ?>
                  |
                  <?php echo $temporal->cantidad_produc; ?>
                </option>
            <?php endforeach; ?>
          <?php else: ?>
            <h3><b>No existe producto</b></h3>
          <?php endif; ?>
      </select>
      <br>
      <b>Vendedor:</b>
      <br>
      <select class="form-control" name="fk_id_ven" id="fk_id_ven" required data-live-search="true">
          <option value="">--Seleccione el Vendedor--</option>
          <?php if ($vendedor): ?>
            <?php foreach ($vendedor->result() as $temporal): ?>
                <option value="<?php echo $temporal->id_ven; ?>">
                  <?php echo $temporal->nombre_ven; ?>
                  |
                  <?php echo $temporal->apellido_ven; ?>
                </option>
            <?php endforeach; ?>
          <?php else: ?>
            <h3><b>No existe producto</b></h3>
          <?php endif; ?>
      </select>
      <br>
      <b>Cantidad:</b>
      <br>
      <select class="form-control" name="fk_id_cant" id="fk_id_cant" required data-live-search="true">
          <option value="">--Seleccione el Cantidad--</option>
          <?php if ($Cantidad): ?>
            <?php foreach ($Cantidad->result() as $temporal): ?>
                <option value="<?php echo $temporal->id_cant; ?>">
                  <?php echo $temporal->unitario_cant; ?>
                  |
                  <?php echo $temporal->paleta_cant; ?>
                </option>
            <?php endforeach; ?>
          <?php else: ?>
            <h3><b>No existe producto</b></h3>
          <?php endif; ?>
      </select>
      <br>
      <b>Fecha:</b>
      <input class="form-control" type="date" name="fecha_fac" id="fecha_fac" value="<?php echo $factura->fecha_fac?>">
      <br>
      <center>
        <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
        <button type="button" class="btn btn-warning " data-dismiss="modal">Close</button>
      </center>
    </form>
    <?php  else: ?>
    <div class="alert alert-danger">
      <b>No se encontro centa</b>
    </div>
    <?php endif; ?>
    <div class="modal-footer">
    </div>
  </div>

    <div class="col-md-2">

    </div>

</div>



<script type="text/javascript">
   $('#fk_id_produc').val("<?php echo $factura->fk_id_produc; ?>");
   $('#fk_id_ven').val("<?php echo $factura->fk_id_ven; ?>");
   $('#fk_id_cant').val("<?php echo $factura->fk_id_cant; ?>");
   $('#fk_id_cant').selectpicker();
   $('#fk_id_produc').selectpicker();
   $('#fk_id_ven').selectpicker();
</script>

<script type="text/javascript">
$("#frm_editar_factura").validate({
   rules:{
       nombre_fac:{
         required:true
       },
       direccion_fac:{
         required: true
       },
       cedula_fac:{
         required: true
       },
       fecha_fac:{
         required:true
       },
       fk_id_produc:{
         required: true
       },
       fk_id_ven:{
         required: true
       },
       fk_id_cant:{
         required: true
         }
   },
   messages:{
     nombre_fac:{
       required:"ingrese el nombre del cliente"
     },
     direccion_fac:{
       required: "ingrese la direccion del cliente"
     },
     cedula_fac:{
       required: "ingrese la cedula"
     },
     fecha_fac:{
       required:"ingrese la fecha"
     },
     fk_id_produc:{
       required: "ingrese el producto"
     },
     fk_id_ven:{
       required: "ingrese al vendedor"
     },
     fk_id_cant:{
       required:"ingrese la cantidad"
     }
   },

});
</script>
