
<?php if ($listadoFactura): ?>
<table id="tbl-factura" class="table table-striped table-bordered table-hover">
<thead>
  <div class="p-3 mb-2 bg-dark text-white">
  <th class="text-center">Id</th>
  <th class="text-center">Nombre</th>
  <th class="text-center">Direccion</th>
  <th class="text-center">Cedula</th>
  <th class="text-center">Fecha</th>
  <th class="text-center">Nombre del vendedor</th>
  <th class="text-center">Nombre del producto</th>
  <th class="text-center">Cantidad</th>
  <th class="text-center">Acciones</th>
</thead>


<tbody>
  <?php foreach ($listadoFactura-> result() as $temporal): ?>
    <tr>
      <td class="text-center"><?php echo $temporal->id_fac; ?></td>
      <td class="text-center"><?php echo $temporal->nombre_fac; ?></td>
      <td class="text-center"><?php echo $temporal->direccion_fac; ?></td>
      <td class="text-center"><?php echo $temporal->cedula_fac; ?></td>
      <td class="text-center"><?php echo $temporal->fecha_fac; ?></td>

      <td class="text-center">
        <?php echo $temporal->nombre_ven; ?>
        <?php echo $temporal->apellido_ven; ?>
      </td>
      <td class="text-center">
        <?php echo $temporal->nombre_produc; ?>
        <?php echo $temporal->cantidad_produc; ?>
      </td>
      <td class="text-center">
        <?php echo $temporal->unitario_cant; ?>
        <?php echo $temporal->paleta_cant; ?>
      </td>
      <td>
        <a href="<?php echo site_url("facturas/editar"); ?>/<?php echo $temporal->id_fac; ?>" class="btn btn-warning" data-toggle="modal" data-target="#moda_editar"> <i class="glyphicon glyphicon-pencil" > Editar</i></a>

        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('facturas/borrar');?>/<?php echo $temporal->id_fac;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>
      </td>
      </tr>
  <?php endforeach; ?>
</tbody>
  </table>
<?php else: ?>

<?php endif; ?>

<script type="text/javascript">
  $('#tbl-factura').DataTable();
</script>
