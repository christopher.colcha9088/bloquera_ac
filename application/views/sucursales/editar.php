<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<div class="row">
  <div class="col-md-12 text-center">

    <div class="bg-success p-2 text-white"><h3>Actualizar Nueva Sucursal</h3></div>

  </div>
</div>
<div class="row">
  <div class="col-md-2">
  </div>
  <div class="col-md-7 text-center ">
    <?php if ($sucursal): ?>
    <form id="frm_nuevo_sucursal" class="" action="<?php echo site_url('sucursales/sucursalEditar'); ?>" method="post">
      <center>
        <input type="hidden" name="id_suc_ac" value="<?php echo $sucursal->id_suc_ac?>">
      </center>
    <label for="">PROVINCIA:</label><br>
    <input type="text" name="provincia_ac"
    value="<?php echo $sucursal->provincia_ac; ?>"
    id="provincia_ac" class="form-control"> <br>

    <label for="">CIUDAD:</label><br>
    <input type="text" name="ciudad_ac"
    value="<?php echo $sucursal->ciudad_ac; ?>"
    id="ciudad_ac" class="form-control"> <br>

    <label for="">ESTADO:</label>
    <br>
    <select class="form-control" name="estado_ac"
    id="estado_ac">
     <option value="">Seleccione una opción</option>
     <option value="ACTIVO">ACTIVO</option>
     <option value="INACTIVO">INACTIVO</option>
    </select>
    <script type="text/javascript">
        $("#estado_ac").val("<?php echo $sucursal->estado_ac;?>");
    </script>
    <br>

    <label for="">DIRECCION:</label><br>
    <input type="text" name="dirreccion_ac"
    value="<?php echo $sucursal->dirreccion_ac; ?>"
    id="dirreccion_ac" class="form-control"> <br>

    <label for="">Email:</label><br>
    <input type="text" name="email_ac"
    value="<?php echo $sucursal->email_ac; ?>"
    id="email_ac" class="form-control"> <br>
    <center>
      <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar </button>
      <button type="button" class="btn btn-warning " data-dismiss="modal"> Close </button>
    </center>
</form>
<?php  else: ?>
<div class="alert alert-danger">
  <b>No se encontro al sucursal</b>
</div>
<?php endif; ?>


<div class="modal-footer">


</div>


</div>

<div class="col-md-2">

</div>

</div>
</div>
