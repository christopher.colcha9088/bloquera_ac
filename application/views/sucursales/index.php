<script type="text/javascript">
  $("#menu-sucursales").addClass('active');
</script>
<div
  class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
  style="background-image: url('https://i.pinimg.com/236x/2a/e0/72/2ae0721952e0612ce8904a1d909492f7.jpg');">

<legend class="text-center">
  <img src="<?php echo base_url() ?>/assets/imagenes/logo1.jpg" alt="Logo" width="90" height="75" class="img-circle">
  <center><div class="bg-success p-2 text-white"> <h3>Gestion de sucursales </h3></div> </center>
</legend>


<?php if ($listadosucursal): ?>
  <table id="tbl-sucursal" class="table table-stripe table-border table-hover">
<thead class="bg-success p-2 text-white">
  <th class="text-center">Id</th>
  <th class="text-center">provincia</th>
  <th class="text-center">ciudad</th>
  <th class="text-center">estado</th>
  <th class="text-center">dirreccion</th>
  <th class="text-center">Email</th>
  <th class="text-center">Acciones</th>
</thead>
<tbody>
  <?php foreach ($listadosucursal-> result() as $temporal): ?>
    <tr>
      <td class="text-center"><?php echo $temporal->id_suc_ac ; ?></td>
      <td class="text-center"><?php echo $temporal->provincia_ac ; ?></td>
      <td class="text-center"><?php echo $temporal->ciudad_ac ; ?></td>
      <td class="text-center"><?php echo $temporal->estado_ac ; ?></td>
      <td class="text-center"><?php echo $temporal->dirreccion_ac ; ?></td>
      <td class="text-center"><?php echo $temporal->email_ac ; ?></td>
      <td>
        <a href="<?php echo site_url("sucursales/editar"); ?>/<?php echo $temporal->id_suc_ac; ?>" class="btn btn-warning" data-toggle="modal" data-target="#moda_editar"> <i class="glyphicon glyphicon-pencil" > Editar</i></a>

        <?php if ($this->session->userdata('conectad0')->perfil_usu=="ADMINISTRADOR"):?>
        <a href="<?php echo site_url('sucursales/borrar');?>/<?php echo $temporal->id_suc_ac;?>" class="btn btn-danger" onclick="return confirm ('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"> Eliminar</i></a>
        <?php endif ?>
      </td>
      </tr>
  <?php endforeach; ?>
</tbody>
  </table>
<?php else: ?>

<?php endif; ?>
<!-- // visualizacion ingreso de datos -->


<!-- Trigger the modal with a button -->
<br>
<center><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_sucursal">Nuevo Sucursal</button></center>

<!-- Modal -->
<div id="modal_sucursal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ingreso de Nueva Sucursal</h4>
      </div>
      <div class="modal-body">
        <form id="frm_nuevo_sucursal" class="" action="<?php echo site_url('sucursales/guardar'); ?>" method="post">
          <b>Provincia:</b>
          <br>
          <input type="text" id="provincia_ac" name="provincia_ac" value="" placeholder="ingrese el nombre del provincia" class="form-control">
          <br>
          <b>Ciudad:</b>
          <br>
          <input type="text" id="ciudad_ac" name="ciudad_ac" value="" placeholder="ingrese la ciudad" class="form-control">
          <br>
          <b>Estado:</b>
          <br>
          <select class="form-control" name="estado_ac"
          id="estado_ac">
           <option value="">Seleccione una opción</option>
           <option value="ACTIVO">ACTIVO</option>
           <option value="INACTIVO">INACTIVO</option>
          </select>
          <br>
          <b>Dirreccion:</b>
          <br>
          <input type="text" id="dirreccion_ac" name="dirreccion_ac" value="" placeholder="ingrese el Direccion" class="form-control">
          <br>
          <b>Email:</b>
          <br>
          <input type="text" id="email_ac" name="email_ac" value="" placeholder="ingrese el Email" class="form-control">
          <br>
          <center>
            <button type="submit" name="button" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
          </center>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal  editar -->
<div id="moda_editar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-edicion">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$("#frm_nuevo_sucursal").validate({
   rules:{
       provincia_ac:{
       minlength:3,
         required: true
       },
       ciudad_ac:{
       minlength:3,
         required: true
       },
       estado_ac:{
         required: true
       },
       dirreccion_ac:{
       minlength:5,
         required: true
       },
       email_ac:{
       minlength:5,
         required: true
       }
   },
   messages:{
     provincia_ac:{
       required: "Ingrese su provincia"
     },
     ciudad_ac:{
       required: "Ingrese su ciudad"
     },
     estado_ac:{
       required: "Ingrese su estado"
     },
     dirreccion_ac:{
       required: "Ingrese su dirreccion"
     },
     email_ac:{
       required: "Ingrese su email"
     }
   },
   submitHandler:function(formulario){
     // ejecutando la peticion asincrona
     $.ajax({
       type:'post',
       url:'<?php echo site_url("sucursales/guardar"); ?>',
       data:$(formulario).serialize(),
       success:function(data){
         var objetoRespuesta=JSON.parse(data);
         if (objetoRespuesta.estado=="ok" || objetoRespuesta.estado=="OK") {
           Swal.fire('Confirmacion',objetoRespuesta.mensaje, 'success'

         );
         $("#modal_sucursal").modal("hide");
         }
         else {
           Swal.fire(
             'ERROR','Error al insertar, intente nuevamente','error'
           );
         }
         // alert(data);
         // $("#modal_sucursal").modal("hide");
       }
     });
   }
});
</script>

<script type="text/javascript">
  $('#tbl-sucursal').DataTable();
</script>

<script type="text/javascript">
    function cargarEdicion(id){
    //  alert("<?php echo site_url('sucursales/editar'); ?>/"+id);
      $("#contenedor-edicion").load("<?php echo site_url('sucursales/editar'); ?>/"+id);
      $("#modal_sucursal").modal("show");
    },

</script>
</div>
