<?php
   class Producto extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("producto",$datos);
     }
     //Funcion que consulta todos los producto de la bdd
     public function obtenerTodos(){
        $productos=$this->db->get("producto");
        if ($productos->num_rows()>0) {
          return $productos;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un producto se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_produc",$id);
        return $this->db->delete("producto");
     }
     //Consultando el producto por su id
     public function obtenerPorId($id){
        $this->db->where("id_produc",$id);
        $producto=$this->db->get("producto");
        if($producto->num_rows()>0){
          return $producto->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de producto
     public function actualizar($id,$datos){
       $this->db->where("id_produc",$id);
       return $this->db->update("producto",$datos);
     }



   }//Cierre de la clase (No borrar)
