<?php
   class Tienda extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
     return $this->db->insert("tienda",$datos);
     }
     //Funcion que consulta todos los tienda de la bdd
     public function obtenerTodos(){
       $this->db->join("vendedor","vendedor.id_ven=tienda.fk_id_ven");
       $this->db->join("producto","producto.id_produc=tienda.fk_id_produc");

        $tiendas=$this->db->get("tienda");
        if ($tiendas->num_rows()>0) {
          return $tiendas;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un tienda se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_tie",$id);
        return $this->db->delete("tienda");
     }

     //Consultando el tienda por su id
     public function obtenerPorId($id){
        $this->db->where("id_tie",$id);
        $tienda=$this->db->get("tienda");
        if($tienda->num_rows()>0){
          return $tienda->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de tienda
     public function actualizar($id,$datos)
     {
       $this->db->where("id_tie",$id);
       return $this->db->update("tienda",$datos);
     }



   }//Cierre de la clase (No borrar)
