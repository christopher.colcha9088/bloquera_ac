<?php
   class Valor extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("valor",$datos);
     }
     //Funcion que consulta todos los profesores de la bdd
     public function obtenerTodos(){
        $valor=$this->db->get("valor");
        if ($valor->num_rows()>0) {
          return $valor;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un profesor se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_val",$id);
        return $this->db->delete("valor");
     }
     //Consultando el profesor por su id
     public function obtenerPorId($id){
        $this->db->where("id_val",$id);
        $valor=$this->db->get("valor");
        if($valor->num_rows()>0){
          return $valor->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de profesores
     public function actualizar($id,$datos){
       $this->db->where("id_val",$id);
       return $this->db->update("valor",$datos);
     }

   }//Cierre de la clase (No borrar)
