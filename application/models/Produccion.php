<?php

class Produccion extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  public function insertar($datos){
    return $this->db->insert("produccion",$datos);
  }


  //funicion que consulta todos los estudiantes de la bdd
  public function obtenerTodas(){
       $this->db->join("producto",
       "producto.id_produc=produccion.fk_id_pro_ac");
       $producciones=$this->db->get("produccion");
       if($producciones->num_rows()>0){
         return $producciones;
       }
       return false;
     }
  // funicion para eliminar un estudiante se recien elid
  public function eliminarPorId($id)
  {
    $this->db->where("id_pro",$id);
    return $this->db->delete("produccion");
  }

  //editar
    public function ObtenerPorId($id)
    {
      $this->db->where("id_pro",$id);
      $produccion=$this->db->get("produccion");
      if ($produccion->num_rows()>0)
      {
        return $produccion->row();
      }
      else
      {
        return false;
      }
    }
    // proceso de actualizacion
    public function editar($id,$datos)
    {
      $this->db->where("id_pro",$id);
      return $this->db->update("produccion",$datos);
    }


  }// cierre de la clase
