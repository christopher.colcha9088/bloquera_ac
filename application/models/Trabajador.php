<?php
class Trabajador extends CI_Model
{
  function __construct()
  {
   parent::__construct();
  }
    public function insertar($datos)
    {
      return $this->db->insert("trabajador",$datos);
    }

    public function obtenerTodos()
    {
      $trabajadores=$this->db->get("trabajador");
      if ($trabajadores->num_rows()>0)
      {
        return $trabajadores;
      }
      else
      {
        return false;
      }
    }

    public function eliminarPorId($id)
    {
    $this->db->where("id_trab",$id);
    return $this->db->delete("trabajador");
    }

    public function obtenerPorId($id)
    {
      $this->db->where("id_trab",$id);
      $trabajador=$this->db->get("trabajador");
      if ($trabajador->num_rows()>0)
      {
        return $trabajador->row();
      }
      else
      {
        return false;
      }
    }

    public function actualizar($id,$datos)
    {
      $this->db->where("id_trab",$id);
      return $this->db->update("trabajador",$datos);
    }

}
