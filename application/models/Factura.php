<?php
   class Factura extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
     return $this->db->insert("factura",$datos);
     }
     //Funcion que consulta todos los factura de la bdd
     public function obtenerTodos(){
       $this->db->join("vendedor","vendedor.id_ven=factura.fk_id_ven");
       $this->db->join("producto","producto.id_produc=factura.fk_id_produc");
       $this->db->join("cantidad","cantidad.id_cant=factura.fk_id_cant");


        $facturas=$this->db->get("factura");
        if ($facturas->num_rows()>0) {
          return $facturas;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un factura se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_fac",$id);
        return $this->db->delete("factura");
     }

     //Consultando el factura por su id
     public function obtenerPorId($id){
        $this->db->where("id_fac",$id);
        $factura=$this->db->get("factura");
        if($factura->num_rows()>0){
          return $factura->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de factura
     public function actualizar($id,$datos)
     {
       $this->db->where("id_fac",$id);
       return $this->db->update("factura",$datos);
     }



   }//Cierre de la clase (No borrar)
