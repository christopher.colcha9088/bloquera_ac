<?php
   class Inventario extends CI_Model
   {
     function __construct()
     {
       parent::__construct();
     }
     public function insertar($datos){
        return $this->db->insert("inventario",$datos);
     }
     //Funcion que consulta todos los inventario de la bdd
     public function obtenerTodos(){
        $inventarios=$this->db->get("inventario");
        if ($inventarios->num_rows()>0) {
          return $inventarios;
        } else {
          return false;//cuando no hay datos
        }
     }
     //funcion para eliminar un inventario se recibe el id
     public function eliminarPorId($id){
        $this->db->where("id_inv",$id);
        return $this->db->delete("inventario");
     }
     //Consultando el inventario por su id
     public function obtenerPorId($id){
        $this->db->where("id_inv",$id);
        $inventario=$this->db->get("inventario");
        if($inventario->num_rows()>0){
          return $inventario->row();//xq solo hay uno
        }else{
          return false;
        }
     }
     //Proceso de actualizacion de configuracion
     public function actualizar($id,$datos)
     {
       $this->db->where("id_inv",$id);
       return $this->db->update("inventario",$datos);
     }



   }//Cierre de la clase (No borrar)
